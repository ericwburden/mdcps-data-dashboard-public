function label_new_reports() {
  var new_indicator = "<sup class='new-label' style='color: #ea4433; font-weight: bold;'> *NEW*</sup>";
  
  var new_reports = [
    "ANE Investigations Management Report",
    "Custody Child Family Visits",
    "Worker Contacts with Custody Child in THV",
    "Worker Contacts with In-Home Children (Yearly)",
    "Foster Homes with No Children Placed",
    "Licensed Home Stats"
  ];
  
  for (var report in new_reports) {
    if ($("ul li a[data-value='" + new_reports[report] + "']").children(".new-label").length === 0) {
      $("ul li a[data-value='" + new_reports[report] + "']").append(new_indicator);
    }
  }
}

function label_updated_reports() {
  var updated_indicator = "<sup class='updated-label' style='color: #14a79d; font-weight: bold;'> ^Updated^</sup>";
  
  var updated_reports = [
  ];
  
  for (var report in updated_reports) {
    if ($("ul li a[data-value='" + updated_reports[report] + "']").children(".updated-label").length === 0) {
      $("ul li a[data-value='" + updated_reports[report] + "']").append(updated_indicator);
    }
  }
}

$(document).on("shiny:value", function(e){
  label_new_reports();
  label_updated_reports();
});

$(document).on("shiny:recalculated", function(e){
  label_new_reports();
  label_updated_reports();
});