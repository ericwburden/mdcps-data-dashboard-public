function setupAccordionListeners() {
  if ($('div.tab-pane.active ul.nav-stacked li.active').length) {
    var show_class = $('div.tab-pane.active ul.nav-stacked li.active').attr('class').split(' ')[1];
    $('li[class*="collapse_grp"]').not("."+show_class).hide();
    $("."+show_class).show();
    $("#"+show_class).addClass('active');
  }
  
  $('button[id*="collapse_grp"]').off();
  $('button[id*="collapse_grp"]').click(function() {
    if (!$(this).hasClass('active')) {
      $('button[id*="collapse_grp"]').removeClass('active');
      $('li[class*="collapse_grp"]').slideUp();
      $("."+this.id).slideDown();
      $(this).addClass('active');
    }
  });
}

$(document).on('shiny:idle', function(event) {
  setupAccordionListeners();
});

$(document).on('shiny:visualchange', function(event){
  setupAccordionListeners();
});

/*
$(document).on('shiny:connected', function() { console.log('shiny:connected'); });
$(document).on('shiny:disconnected', function() { console.log('shiny:disconnected'); });
$(document).on('shiny:sessioninitialized', function() { console.log('shiny:sessioninitialized'); });
$(document).on('shiny:busy', function() { console.log('shiny:busy'); });
$(document).on('shiny:idle', function() { console.log('shiny:idle'); });
$(document).on('shiny:inputchanged', function() { console.log('shiny:inputchanged'); });
$(document).on('shiny:message', function() { console.log('shiny:message'); });
$(document).on('shiny:conditional', function() { console.log('shiny:conditional'); });
$(document).on('shiny:bound', function() { console.log('shiny:bound'); });
$(document).on('shiny:unbound', function() { console.log('shiny:unbound'); });
$(document).on('shiny:value', function() { console.log('shiny:value'); });
$(document).on('shiny:error', function() { console.log('shiny:error'); });
$(document).on('shiny:outputinvalidated', function() { console.log('shiny:outputinvalidated'); });
$(document).on('shiny:recalculating', function() { console.log('shiny:recalculating'); });
$(document).on('shiny:recalculated', function() { console.log('shiny:recalculated'); });
$(document).on('shiny:visualchange', function() { console.log('shiny:visualchange'); });
$(document).on('shiny:updateinput', function() { console.log('shiny:updateinput'); });
$(document).on('shiny:filedownload', function() { console.log('shiny:filedownload'); });
//*/