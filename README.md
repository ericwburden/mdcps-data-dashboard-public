# Overview

This repository contains an R/Shiny application that sets up a content
management system for retrieving data from a SQL Server and displaying that data
as a set of interactive reports. Tailored for use by the Mississippi Department
of Child Protection Services, MACWIS Database system. The application sets up an
independent RServe instance to periodically monitor the SQL Server for updates, 
which then updates locally stored data files from which the reported data is
drawn.
