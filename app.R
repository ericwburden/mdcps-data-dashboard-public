# Imports -----------------------------------------------------------------

# List of libraries. Consolidate all library calls here, to help with setup
# for the server.
library(shiny)
library(shinythemes)
library(shinyWidgets)
library(shinyBS)
library(shinyjs)
library(tidyverse)
library(DT)
library(lubridate)
library(magrittr)
library(yaml)
library(RODBC)
library(lazyeval)
library(rmarkdown)
library(assertthat)
library(Cairo)
library(grid)
library(gridExtra)
library(htmltools)
library(xlsx)
library(Rserve)
library(RSclient)
library(stringr)
library(slackr)
options(shiny.usecairo=T) #This option improves plot quality

# The source file for the code that reads the report query and manifest
# from the reports folder, and builds the report list
source('modules/script_modules/helper_module.R')
source('modules/script_modules/monitor_module.R')
source('modules/script_modules/report_module.R')
source('modules/ui_modules/header.R')
source('modules/ui_modules/tab_modules.R')
source('modules/ui_modules/timestamp_module.R')

## Updater for worker_list and coreg_list -------------------------------------
update_context_list_observer <- observe(domain =NULL, {
  if (!dir.exists('modules/data_modules')) dir.create('modules/data_modules')
  
  invalidateLater(1000*60*60, session = NULL)
  asws_wrkr_query <- paste(
    'select [ASWS Name],
    [Worker Name],
    [Work Type]
    from [EWB_All_Workers_By_Type_Dev]'
  )
  
  coreg_query <- paste(
    'select RTRIM(county_name) as \'County\',
      RTRIM(region_name) as \'Region\'
    from COUNTY_REGION
    where region_name <> \'STATEWIDE\''
  )
  
  query_db <- RODBC::odbcConnect(
    'EDW',
    uid = 'fcsadmin',
    pwd = 'Welcome1'
  )
  
  #Run the query
  RODBC::sqlQuery(query_db, asws_wrkr_query) %>%
    saveRDS('modules/data_modules/worker_list.RDS')
  
  RODBC::sqlQuery(query_db, coreg_query) %>%
    filter(
      Region %in% c(
          'I-N',  'I-S', 'II-E', 'II-W', 'III-N', 'III-S', 'IV-N',
          'IV-S', 'V-E', 'V-W',  'VI',   'VII-C', 'VII-E', 'VII-W'
        )
    ) %>%
    mutate(
      Region = factor(
        Region,
        levels = c(
          'I-N',  'I-S', 'II-E', 'II-W', 'III-N', 'III-S', 'IV-N',
          'IV-S', 'V-E', 'V-W',  'VI',   'VII-C', 'VII-E', 'VII-W'
        )
      )
    ) %>%
    saveRDS('modules/data_modules/coreg_list.RDS')
  
  #Close the connection
  RODBC::odbcClose(query_db)
})

## Unpack reports globally ----------------------------------------------------
#The list of report 'objects'
reports <- list()

reports_meta <- list(
  'Categories' = c(),
  'Reports' = list()
)

#Unpack the report manifests
for (dir in list.files('reports')) {
  #Only operate on directories
  if(
    !assertthat::is.dir(
      paste('reports', dir, sep = '/')
    )
  ) (next)
  
  #Unpack the report manifest
  reports[[dir]] <- unpack_report_manifest(dir)
  
  #Collect all worker types
  if (!(reports[[dir]]$`Worker Type` %in% reports_meta$`Worker Types`)) {
    reports_meta$`Worker Types` <- c(reports_meta$`Worker Types`, reports[[dir]]$`Worker Type`)
  }
  
  #If category isn't already recorded
  if(!(reports[[dir]]$Category %in% names(reports_meta$Categories))) {
    #print(reports[[dir]]$Category)
    reports_meta$Categories[[reports[[dir]]$Category]] <- list(
      'name' = reports[[dir]]$Category,
      'Headings' = reports[[dir]]$Heading
    )
  } else {
    #If the heading isn't also already recorded
    if(!(
      reports[[dir]]$Heading %in% 
      reports_meta$Categories[[reports[[dir]]$Category]]$Headings
    )) {
      reports_meta$Categories[[reports[[dir]]$Category]]$Headings <- c(
        reports_meta$Categories[[reports[[dir]]$Category]]$Headings,
        reports[[dir]]$Heading
      )
    }
  }
  
  #Save report spec html to reports metadata
  reports_meta$Reports[[reports[[dir]]$`Report Name`]] <- list(
    `Spec` = reports[[dir]]$SpecHTML,
    `Tag` = dir,
    `Filter Type` = reports[[dir]]$`Filter Type`
  ) 

}
dir.create('modules/data_modules', showWarnings = F)
write(as.yaml(reports_meta), file = 'modules/data_modules/reports_metadata.yaml')

## Global UI Framework --------------------------------------------------------

ui_list <- list()
for (category in names(reports_meta$Categories)) {
  if (category != 'Development') {
    ui_list[[length(ui_list) + 1]] <- report_tab(category)
  }
}
ui_list[['title']] <- 'MDCPS Focus on Data'
ui_list[['collapsible']] <- TRUE
ui_list[['theme']] <- 'css/bootstrap.css'
ui_list[['id']] <- 'active_nav_panel'
ui_list[['header']] <- header()
main_ui <- do.call(navbarPage, ui_list)

## Data monitor Setup ---------------------------------------------------------

observe(
  domain = NULL,
  {
    invalidateLater(1000*60*5, session = NULL)
    data_monitor <- data_monitor_init()
    data_monitor_scan(data_monitor)
    RS.close(data_monitor)
  }
)


# User Interface ----------------------------------------------------------
ui <- tagList(
  
  #Extra tags to include in the head, such as external javascript and css
  tags$head(
    tags$link(
      rel = 'stylesheet',
      type = 'text/css',
      href = 'css/loading.css'
    ),
    tags$link(
      rel = 'stylesheet',
      type = 'text/css',
      href = 'css/modal.css'
    ),
    # Favicon tags
    tags$link(
      rel = 'icon',
      href = 'images/favicon-16x16.png',
      sizes = '16x16',
      type = 'image/png'
    ),
    tags$link(
      rel = 'icon',
      href = 'images/favicon-32x32.png',
      sizes = '32x32',
      type = 'image/png'
    ),
    tags$link(
      rel = 'icon',
      href = 'images/favicon-96x96.png',
      sizes = '96x96',
      type = 'image/png'
    ),
    tags$link(
      rel = 'apple-touch-icon',
      sizes = '57x57',
      href = 'images/apple-icon-57x57.png'
    ),
    tags$link(
      rel = 'apple-touch-icon',
      sizes = '60x60',
      href = 'images/apple-icon-60x60.png'
    ),
    tags$link(
      rel = 'apple-touch-icon',
      sizes = '72x72',
      href = 'images/apple-icon-72x72.png'
    ),
    tags$link(
      rel = 'apple-touch-icon',
      sizes = '76x76',
      href = 'images/apple-icon-76x76.png'
    ),
    tags$link(
      rel = 'apple-touch-icon',
      sizes = '114x114',
      href = 'images/apple-icon-114x114.png'
    ),
    tags$link(
      rel = 'apple-touch-icon',
      sizes = '120x120',
      href = 'images/apple-icon-120x120.png'
    ),
    tags$link(
      rel = 'apple-touch-icon',
      sizes = '144x144',
      href = 'images/apple-icon-144x144.png'
    ),
    tags$link(
      rel = 'apple-touch-icon',
      sizes = '152x152',
      href = 'images/apple-icon-152x152.png'
    ),
    tags$link(
      rel = 'apple-touch-icon',
      sizes = '180x180',
      href = 'images/apple-icon-180x180.png'
    ),
    tags$link(
      rel = 'icon',
      type = 'image/png',
      sizes = '192x192',
      href = 'images/android-icon-192x192.png'
    ),
    tags$link(
      rel = 'icon',
      type = 'image/png',
      sizes = '96x96',
      href = 'images/android-icon-96x96.png'
    ),
    tags$link(
      rel = 'icon',
      type = 'image/png',
      sizes = '32x32',
      href = 'images/android-icon-32x32.png'
    ),
    tags$link(
      rel = 'icon',
      type = 'image/png',
      sizes = '16x16',
      href = 'images/android-icon-16x16.png'
    ),
    tags$link(
      rel = 'manifest',
      href = 'images/manifest.json'
    ),
    tags$meta(
      name = 'msapplication-TileColor',
      content = '#ffffff'
    ),
    tags$meta(
      name = 'msapplication-TileImage',
      content = 'images/ms-icon-144x144.png'
    ),
    tags$meta(
      name = 'theme-color',
      content = '#ffffff'
    ),
    # Apple Web App meta tags
    tags$meta(
      name = 'apple-mobile-web-app-capable',
      content = 'yes'
    ),
    tags$script(
      src = 'js/dimension.js'
    ), #Script to automagically resize table for various screen sizes
    tags$script(
      src = 'js/new_reports.js'
    )
  ),
  
  useShinyjs(),
  
  # Generated in Global UI Framework section
  main_ui,
  tags$script(
    src = 'js/toggle_report_menus.js'
  ),
  update_timestamp()
)

# Server ----------------------------------------------------------------------
server <- function(input, output, session) {
  ## Initial Setup ------------------------------------------------------------
  rv <- reactiveValues()
  
  rv$loading <- TRUE
  output$hide_loader <- reactive({
    return(
      !rv$loading
    )
  })
  outputOptions(output, 'hide_loader', suspendWhenHidden=FALSE)
  
  app_user_file <- file('modules/data_modules/app_user', open = 'w')
  write(
    isolate(
      paste0(
        session$clientData$url_hostname,
        ':',
        session$clientData$url_port
      )
    ),
    file = app_user_file
  )
  close(app_user_file)
  
  ## Manifest Management ------------------------------------------------------
  
  #Bind reports to input/output
  bound_reports <- list()
  for (report in reports) {
    bound_reports[[report$`Report Tag`]] <- register_report_bindings(report, input, output)
  }
  
  rv$report_category_inputs <- list()
  for (category in names(reports_meta$Categories)) {
    local({
      local_category <- category
      
      output_tag <- paste(
        gsub(' ', '_', tolower(category)),
        'reports',
        sep = '_'
      )
      
      output_tabs <- paste(
        gsub(' ', '_', tolower(category)),
        'tabs',
        sep = '_'
      )
      
      output[[output_tag]] <- renderUI({
        selected_report <- input[[output_tabs]]
        
        # Render the report code into the navlistPanel
        nav_list <- list()
        for (heading in reports_meta$Categories[[local_category]]$Headings) {
          nav_list[[length(nav_list) + 1]] <- heading
          for (report in bound_reports) {
            #print(report$`Report Name`)
            if (
              report$Heading == heading & 
              report$Category == local_category
            ) {
              nav_list[[length(nav_list) + 1]] <- report$`Report UI Elements`()
            }
          }
        }
        
        #Additional options to pass to navlistPanel
        nav_list[['id']] <- output_tabs
        nav_list[['widths']] <- c(2, 10)
        nav_list[['fluid']] <- F
        nav_list[['well']] <- F
        
        #Pass the selected report as a param, to keep from defaulting to the first
        #report
        if(!is.null(selected_report)) {nav_list[['selected']] <- selected_report}
        rv$loading <- FALSE
        
        collapsible_navlist(do.call(navlistPanel, nav_list), output_tag)
      })
    })
  }
  
  # The active report name
  active_report <- reactive({
    
    #Determine which report set we're using. Each report set (navListPanel),
    #should have an id like lowercase_report_set_name_tabs. For example, the
    #Direct Service report set has the id direct_service_tabs
    report_type <- paste(
      gsub(
        ' ', 
        '_', 
        tolower(input$active_nav_panel)
      ),
      'tabs',
      sep = '_'
    )
    
    input[[report_type]]
  })
  
  active_report_tag <- reactive({
    reports_meta$Reports[[active_report()]]$Tag
  })
  
  observeEvent(active_report(), {
    # When the report changes, update the available filters

    switch(
      reports_meta$Reports[[active_report()]]$`Filter Type`,
      'Context' = runjs('
         $("#filter_type").removeClass("hidden");
         $("#filter").removeClass("hidden");
         $("#date_filter").addClass("hidden");
      '),
      'Time' = runjs('
         $("#filter_type").addClass("hidden");
         $("#filter").addClass("hidden");
         $("#date_filter").removeClass("hidden");
      '),
      'All' = runjs('
         $("#filter_type").removeClass("hidden");
         $("#filter").removeClass("hidden");
         $("#date_filter").removeClass("hidden");
       ')
    )
  })
  
## Set Up Context Selectors ---------------------------------------------------
  rv$filter <- list(
    Statewide = 'ALL',
    Region = 'ALL',
    County = 'ALL',
    ASWS = 'ALL',
    Worker = 'ALL'
  )
  
  asws_worker_list <- reactiveFileReader(
    1000*60*15,
    session = session,
    filePath = 'modules/data_modules/worker_list.RDS',
    readFunc = readRDS
  )
  
  coreg_list <- reactiveFileReader(
    1000*60*15,
    session = session,
    filePath = 'modules/data_modules/coreg_list.RDS',
    readFunc = readRDS
  )
  
  observeEvent(input$filter_type, {
    if (input$filter_type == 'Statewide') {
      rv$filter <- list(
        Statewide = 'ALL',
        Region = 'ALL',
        County = 'ALL',
        ASWS = 'ALL',
        Worker = 'ALL'
      )
    }
    
    output$filter <- renderUI({
      selectInput(
        'filter',
        'Filter',
        choices = {
          switch(
            input$filter_type,
            Statewide = c('ALL'),
            Region = c(
              'ALL',
              coreg_list() %>%
                arrange(Region) %>%
                .$Region %>%
                unique() %>%
                as.character()
            ),
            County = c(
              'ALL',
              coreg_list() %>%
                arrange(County) %>%
                .$County %>%
                unique() %>%
                as.character()
            ),
            ASWS = c(
              'ALL',
              asws_worker_list() %>%
                filter(
                  `Work Type` == input$active_nav_panel
                ) %>%
                arrange(`ASWS Name`) %>%
                .$`ASWS Name` %>%
                unique() %>%
                as.character()
            ),
            Worker = c(
              'ALL',
              asws_worker_list() %>%
                filter(
                  `Work Type` == input$active_nav_panel
                ) %>%
                arrange(`Worker Name`) %>%
                .$`Worker Name` %>%
                unique() %>%
                as.character()
            )
          )
        }
      )
    })
    
  })
  
  observeEvent(input$filter, {
    rv$filter <- list(
      Statewide = 'ALL',
      Region = 'ALL',
      County = 'ALL',
      ASWS = 'ALL',
      Worker = 'ALL'
    )
    
    if (input$filter_type != 'Statewide') {
      rv$filter[[input$filter_type]] <- input$filter
    }
  })
  
  # If the navBarPanel name matches a worker type, show the ASWS
  # and Worker filters, otherwise, only show the Statewide, Region
  # and County filters
  observeEvent(input$active_nav_panel, {
    worker_types <- as.character(unique(asws_worker_list()$`Work Type`))
    if (input$active_nav_panel %in% worker_types) {
      runjs(
        '$("input[name=filter_type][value=ASWS]").parent().show();
         $("input[name=filter_type][value=Worker]").parent().show();
         $("input[name=filter_type][value=Region]").parent().show();'
      )
      if (input$active_nav_panel == 'Special Investigations') {
        runjs(
          '$("input[name=filter_type][value=Region]").parent().hide();'
        ) 
      }
    } else {
      runjs(
        '$("input[name=filter_type][value=ASWS]").parent().hide();
         $("input[name=filter_type][value=Worker]").parent().hide();
         $("input[name=filter_type][value=Region]").parent().show();'
      )
    }
  })
  
  observeEvent(
    active_report(),
    {
      add_filter_field <- bound_reports[[
        active_report_tag()
      ]]$`Additional Filter`
      
      print(add_filter_field)

      if(!is.null(add_filter_field)) {
        updateSelectInput(
          session,
          'add_filter',
          label = add_filter_field,
          choices = c(
            'ALL',
            as.character(
              unique(
                bound_reports[[
                  active_report_tag()
                  ]]$`Base Data`()[[add_filter_field]]
              )
            )
          )
        )
        runjs(
          '$("#add_filter").parent().parent().show();'
        )
      } else {
        runjs(
          '$("#add_filter").parent().parent().hide();'
        )
      }
    }
  )
  
  ## User Access Logging ------------------------------------------------------
  session_id <- paste(stringi::stri_rand_strings(10, 10), collapse = '')
  
  if (!dir.exists('logs')) {
    dir.create('logs')
  }
  
  if (!file.exists('logs/user_access.log')) {
    file.create('logs/user_access.log')
  }
  
  access_log_file <- file('logs/user_access.log', open = 'a')
  
  # Log initial access
  observe({
    rv$uid <- getQueryString(session)$ui
    if(!is.null(rv$uid)) {
      cat(
        paste(
          '\n',
          lubridate::now(), 
          session_id,
          'access',
          rv$uid
        ),
        file = access_log_file
      )
    }
  })
  
  # Log category access
  observeEvent(input$active_nav_panel, {
    if(!is.null(rv$uid)) {
      cat(
        paste(
          '\n',
          lubridate::now(),
          session_id,
          'category',
          rv$uid,
          gsub(' ', '_', input$active_nav_panel)
        ),
        file = access_log_file
      )
    }
  })
  
  # Log report access
  observeEvent(active_report(), {
    if(!is.null(rv$uid)) {
      cat(
        paste(
          '\n',
          lubridate::now(), 
          session_id,
          'report',
          rv$uid,
          gsub(' ', '_', active_report())
        ),
        file = access_log_file
      )
    }
  })
  
  # Log session end
  session$onSessionEnded(function() {
    if(!is.null(isolate(rv$uid))) {
      cat(
        paste(
          '\n',
          lubridate::now(), 
          session_id,
          'end',
          isolate(rv$uid)
        ),
        file = access_log_file
      )
    }
    
    close(access_log_file)
  })
  
  ## Button Obervers ------------------------------------------------------------
  observeEvent(input$help_button, {
    
    reports_meta <- yaml.load_file('modules/data_modules/reports_metadata.yaml')
    
    # Get the report help content based on the active tab
    # from the active report set
    help_content <- reports_meta$Reports[[active_report()]]$Spec
    
    #Show the help dialog
    showModal(modalDialog(
      title = 'Report Help',
      htmlTemplate(text_ = help_content),
      easyClose = T,
      footer = tagList(
        downloadButton(
          'md_dl_rules', 
          'Download Business Rules',
          class = 'btn btn-info'
        ),
        modalButton('Dismiss')
      ),
      size = 'l'
    ))
    
    runjs('$("#business-rules table").addClass("table table-bordered")')
  })
  
  # Render the displayed table into an rmarkdown template and download it
  output$dl_pdf <- downloadHandler(
    filename = function() {
      paste0(now(), ' ', active_report(), '.pdf')
    },
    content = function(file) {
      
      report_tag <- reports_meta$Reports[[active_report()]]$Tag
      
      tempReport <- file.path(tempdir(), 'printable_format.Rmd')
      file.copy('modules/ui_modules/printable_format.Rmd', tempReport, overwrite = TRUE)
      
      output_data <- data.frame(
        lapply(
          bound_reports[[report_tag]]$`Filtered Data`(),
          function(x) {
            gsub('<br/>', '\n', x)
          }
        ),
        check.names = F
      )
      
      # Params to pass to the template, including report data
      params = list(
        name = active_report(),
        date = bound_reports[[report_tag]]$`Last Data Refresh`(),
        content = list(
          row2 = list(
            type = 'table',
            column1 = output_data
          )
        )
      )
      
      #Lock screen with a modal Dialog while the report is rendering
      showModal(modalDialog(
        title = 'Preparing Report',
        'Preparing PDF report...',
        size = 's',
        easyClose = F,
        footer = NULL
      ))
      
      file_output <- rmarkdown::render(
        tempReport,
        output_file = file,
        params = params,
        envir = new.env(parent = globalenv())
      )
      
      removeModal() # Remove the modal once the report is rendered
      
      file_output
    }
  )
  
  # Render the displayed table into an excel spreadsheet and download it
  output$dl_excel <- downloadHandler(
    filename = function() {
      paste0(now(), ' ', active_report(), '.xlsx')
    },
    content = function(file) {
      
      report_tag <- reports_meta$Reports[[active_report()]]$Tag
      
      #Lock screen with a modal Dialog while the report is rendering
      showModal(modalDialog(
        title = 'Preparing Report',
        'Preparing Excel report...',
        size = 's',
        easyClose = F,
        footer = NULL
      ))
      
      output_data <- data.frame(
        lapply(
          bound_reports[[report_tag]]$`Filtered Data`(),
          function(x) {
            gsub('<br/>', ' \r\n', x)
          }
        ),
        check.names = F
      )
      
      file_output <- write.xlsx(
        output_data, 
        file,
        row.names = F
      )
      
      removeModal() # Remove the modal once the report is rendered
      
      file_output
    }
  )
  
  # Render the displayed table into an rmarkdown template and download it
  business_rule_downloader <- function() {
    downloadHandler(
      filename = function() {
        paste(active_report(), 'Business Rules.docx')
      },
      content = function(file) {
        
        report_tag <- reports_meta$Reports[[active_report()]]$Tag
        
        file_path <- paste(
          'reports',
          report_tag,
          paste0(report_tag, '_specs.Rmd'),
          sep = '/'
        )
        
        #Lock screen with a modal Dialog while the report is rendering
        showModal(modalDialog(
          title = 'Preparing Report',
          'Preparing Business Rules...',
          size = 's',
          easyClose = F,
          footer = NULL
        ))
        
        file_output <- rmarkdown::render(
          file_path,
          output_file = file,
          output_format = 'word_document',
          envir = new.env(parent = globalenv())
        )
        
        removeModal() # Remove the modal once the report is rendered
        
        file_output
      }
    )
  }
  
  # Download button in the download button group
  output$dl_rules <- business_rule_downloader()
  
  # Download button in the report info modal
  output$md_dl_rules <- business_rule_downloader()

  observeEvent(input$reset_filters, {
    updateDateRangeInput(session, 'date_filter', start = NA, end = NA)
    
    updateSelectInput(session, 'filter', selected = 'ALL')
    
    updateSelectInput(session, 'add_filter', selected = 'ALL')
  })
}

# Run the application 
shinyApp(ui = ui, server = server)
