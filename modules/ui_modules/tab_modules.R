report_tab <- function(title) {
  output_tag <- paste(
    gsub(' ', '_', tolower(title)),
    'reports',
    sep = '_'
  )
  
  tabPanel(
    title,
    value = title,
    fluidRow(
      uiOutput(output_tag)
    )
  )
}

# This function takes an existing navlistPanel object, and replaces the
# .navbar-brand headings with buttons to allow the subsequent list items
# to be shown or hidden on click. The toggle functionality is provided by
# the javascript file toggle_report_menus.js
collapsible_navlist <- function(original_navlist, category) {
  group_num <- 0 #start with 0, first group will be group 1
  
  # For every item in the navlist
  for (i in 1:length(original_navlist$
                     children[[1]][[1]]$
                     children[[1]]$
                     children[[1]])) {
    
    # Get item current attributes
    current_attribs <- original_navlist$
      children[[1]][[1]]$
      children[[1]]$
      children[[1]][[i]]$
      attribs
    
    # Check if item is a heading (navbar-brand)
    new_heading <- FALSE
    if(length(current_attribs) > 0) {
      if (current_attribs == 'navbar-brand'){
        new_heading <- TRUE
      }
    }

    # For headings...
    if (new_heading) {

      # Update the group number, set the li class to NULL, convert the
      # li child string to a button, set the button classes and id
      group_num <- group_num + 1
      new_class <- NULL
      
      button_text <- original_navlist$
        children[[1]][[1]]$
        children[[1]]$
        children[[1]][[i]]$
        children[[1]]
      
      original_navlist$
        children[[1]][[1]]$
        children[[1]]$
        children[[1]][[i]]$
        children[[1]] <- actionButton(
          paste0(category, '_collapse_grp_', group_num),
          button_text
        )
      
      original_navlist$
        children[[1]][[1]]$
        children[[1]]$
        children[[1]][[i]]$
        children[[1]]$
        attribs$class <- 'btn action-button btn-success btn-block'
      
    } else { # For non-headings...
      #Set the li class to the same as the button id
      new_class <- paste(
        original_navlist$
          children[[1]][[1]]$
          children[[1]]$
          children[[1]][[i]]$
          attribs$class,
        paste0(category, '_collapse_grp_', group_num)
      )
      
      # For groups after the first, hide by default
      if (group_num > 1) {
        original_navlist$
          children[[1]][[1]]$
          children[[1]]$
          children[[1]][[i]]$
          attribs$style <- 'display:none;'
      }
    }

    # Set li classes
    original_navlist$
      children[[1]][[1]]$
      children[[1]]$
      children[[1]][[i]]$
      attribs$class <- new_class
  }

  original_navlist
}

