with query as (
	select distinct c.region,
		c.county,
		RTRIM(RTRIM(C.super_last) + ', ' + RTRIM(C.super_first) + ' ' + ISNULL(RTRIM(C.super_m), '')) asws,
		RTRIM(RTRIM(C.worker_last) + ', ' + RTRIM(C.worker_first) + ' ' + ISNULL(RTRIM(C.worker_m), '')) cor_worker,
		a.case_id,
		a.case_start_date,
		a.pers_id member_id,
		a.member_name,
		DATEDIFF(YEAR,a.pers_dob, GETDATE()) age,
		--a.member_type,
		a.cust_start_date,
		a.days_in_cust,
		d.placement_status,
		d.placement_start_date,
		d.days_in_placement,
		d.resource_name,
		case
			when d.facility_type = 9000 then 'Placement Services'
			when d.facility_type = 9010 then 'Acute Care'
			when d.facility_type = 9020 then 'Foster Home'
			when d.facility_type = 9030 then 'Residential Treatment'
			when d.facility_type = 9040 then 'Emergency Foster Home'
			when d.facility_type = 9050 then 'ICFMR'
			when d.facility_type = 9060 then 'Therapeutic Foster Home'
			when d.facility_type = 9070 then 'Maternity Home'
			when d.facility_type = 9080 then 'Foster/Adopt Foster Home'
			when d.facility_type = 9090 then 'Relative Foster Home'
			when d.facility_type = 9100 then 'Chemically Dependent Group'
			when d.facility_type = 9110 then 'Adoption Unit Foster Home'
			when d.facility_type = 9120 then 'Specialized Residential School'
			when d.facility_type = 9130 then 'Adoptive Home (Domestic)'
			when d.facility_type = 9140 then 'Medical Treatment Group Home'
			when d.facility_type = 9150 then 'Respite Foster Home'
			when d.facility_type = 9170 then 'Medical/Treatment Foster Home'
			when d.facility_type = 9190 then 'Relative Foster Home'
			when d.facility_type = 9210 then 'Teenage Parent Foster Home'
			when d.facility_type = 9230 then 'Contract Facility - Non MDHS'
			when d.facility_type = 9250 then 'Group Home'
			when d.facility_type = 9260 then 'Therapeutic Group Home'
			when d.facility_type = 9270 then 'Licensed Facility'
			when d.facility_type = 9275 then 'ICPC - Incoming'
			when d.facility_type = 9280 then 'ICPC - Outgoing'
			when d.facility_type = 9290 then 'Child-specific'
			when d.facility_type = 9300 then 'Runaway'
			when d.facility_type = 9305 then 'CO Non Licensed Det/Trng School'
			when d.facility_type = 9310 then 'Own Home'
			when d.facility_type = 9315 then 'CO Non Licensed Shelter'
			when d.facility_type = 9320 then 'Child Placing Agency'
			when d.facility_type = 9330 then 'Emergency Shelter'
			when d.facility_type = 9340 then 'Residential Child Caring Facility'
			when d.facility_type = 9350 then 'Institution'
			when d.facility_type = 9360 then 'Supervised Independent Living'
			when d.facility_type = 9370 then 'Adoption (International)'
			when d.facility_type = 9375 then 'CO Non Licensed Relative'
			when d.facility_type = 9380 then 'CO Non Licensed Non-Relative'
			when d.facility_type = 9385 then 'Interim Placement'
			when d.facility_type = 9390 then 'Own Home - Mother'
			when d.facility_type = 9395 then 'Own Home - Father'
			when d.facility_type = 9400 then 'Own Home - Parents'
			when d.facility_type = 9405 then 'Own Home - Other Caretaker'
			when d.facility_type = 9410 then 'Expeditied Pending Relative'
			else d.facility_type
		end facility_type,
		case 
		  when d.facility_type in (9310,9390,9395,9400,9405) then 'Y'
		  else 'N'
		end trial_home
	from CUSTODY_MEMBERS_TEST a 
	join CUSTODY_WORKERS_TEST c 
	on a.case_id = c.case_id
		and a.pers_id = C.pers_id
	left join CUSTODY_PLACEMENTS_TEST d 
	on a.pers_id = d.pers_id
		and d.placement_status in ('A','P')    
	left join CUSTODY_PERM_PLAN_TEST e 
	on a.pers_id = e.pers_id 
)

select GETDATE() as 'Data Date', 
  rtrim(region) as 'Region',
  RTRIM(county) as 'County',
  asws as 'ASWS Name',
  cor_worker as 'Worker Name',
  member_id as 'Child ID',
  member_name as 'Child Name',
  resource_name as 'Resource Name',
  isnull(facility_type, 'No Placement') as 'Facility Type',
  trial_home as 'Trial Home Placement',
  isnull(placement_status, 'N') as 'Placement Status',
  isnull(days_in_placement, 0) as 'Days in Placement',
  case
    when placement_status is null then 2
    when cor_worker is null then 1
    else 0
  end as 'Indicator Column'
from query
