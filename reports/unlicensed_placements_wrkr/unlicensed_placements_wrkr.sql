SELECT distinct GETDATE() as 'Data Date',
	rtrim(region) as 'Region',
    rtrim(county) as 'County',
    RTRIM(RTRIM(sup_last) + ', ' + RTRIM(sup_first) + ' ' + ISNULL(RTRIM(sup_init), '')) as 'ASWS Name',
    RTRIM(RTRIM(work_last) + ', ' + RTRIM(work_first) + ' ' + ISNULL(RTRIM(work_init), '')) as 'Worker Name',
    child_id as 'Child ID',
    upper(ltrim(rtrim(child_last)) + ', ' + ltrim(rtrim(child_first))) as 'Child Name',
    plac_st_dt as 'Placement Start Date',
	plac_days as 'Days in Placement',
	upper(plac_res_name) as 'Resource Name',
	rtrim(resource_county) as 'Resource County',
	plac_type as 'Placement Type',
	ltrim(rtrim(res_wkr_last)) + ', ' + ltrim(rtrim(res_wkr_first)) as 'Resource Worker'
FROM SLS319_WKR_BLENDER 
