 select Distinct GETDATE() as 'Data Date',
     a.region as 'Region',
     a.county as 'County',
	  a.ASWS as 'ASWS Name',
	  a.Worker as 'Worker Name',
      a.[Child ID] as 'Child ID',
      a.[Child Name] as 'Child Name',
	  f.legally_free_date as 'Legally Free Date',
	  DATEDIFF(D, f.legally_free_date, getdate()) as 'Days Legally Free' 
	-- g.adopt_final_court_date
 
from review_track a
 
   left join review_placements_track_adopt d on a.[Child ID] = d.[Child ID]
         and d.placement_status = 'A'
    left join  review_legally_free_track f on a.[Child ID] = f. [Child ID]
	left join review_plan_track e on a.[Child ID] = e.[Child ID]
   left join review_adopt_final_track g on a.[Child ID] = g.[Child ID] --and f.fsp_id=g.fsp_id
   where f.legally_free_date is not null and g.adopt_final_court_date is null
   order by region,county,ASWS,Worker