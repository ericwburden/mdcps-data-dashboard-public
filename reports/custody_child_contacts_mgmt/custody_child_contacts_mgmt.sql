set nocount on;

--exec fod_gen_child_case_crosswalk_yearly
--exec fod_gen_contact_table_yearly
--exec fod_gen_worker_child_visitation_yearly

select [Data Date],
	Region,
	County,
	[ASWS Name],
	[Worker Name],
	[Period Name],
	[Child Name],
	[First Monthly Contact Date],
	[Recent Monthly Contact Date],
	[Days Since Last Visit],
	[Contacts This Month],
	[Contacts This Month (In Home)],
	[Indicator Column]
from fod_worker_child_visitation_yearly