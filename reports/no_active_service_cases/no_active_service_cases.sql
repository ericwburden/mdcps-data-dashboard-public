/*
create function dbo.EWB_Cases_With_No_Services_Report()
returns @cases_w_no_services table (
	[Data Date] datetime,
	Region varchar(30),
	County varchar(30),
	[ASWS Name] varchar(100),
	[Worker Name] varchar(100),
	[Case ID] int,
	[HH Name] varchar(100),
	[Case Start] date,
	[Days Without Services] int,
	[Estimated Case Weight] float
)
as
begin

	declare @custody_children_per_case table (
		[Children in Custody] int,
		[Case ID] int
	);

	insert into @custody_children_per_case
	select COUNT(*) as 'Children in Custody',
		[Case ID]
	from dbo.Custody_Case_Crosswalk_Fun(GETDATE(), GETDATE())
	group by [Case ID];

	insert into @cases_w_no_services
	select GETDATE() as 'Data Date',
		Region,
		County,
		[ASWS Name],
		[Worker Name],
		[Case ID],
		[HH Name],
		[Case Start],
		DATEDIFF(DD, isnull([FSP Approval Date], [Case Start]), GETDATE()) as 'Days Without Services',
		CASE
			WHEN [Children in Custody] IS NULL THEN 0.0588
			ELSE [Children in Custody] * 0.0714
		END AS 'Estimated Case Weight'
	from (
		select base.[Case ID],
			convert(varchar, coreg.Region) as 'Region',
			convert(varchar, coreg.County) as 'County',
			wrkr.[Worker Name], 
			wrkr.[Worker ID],
			asws.[ASWS Name],
			hh.[HH ID],
			hh.[HH Name],
			case_start.[Case Start],
			fsp.[Plan Type],
			fsp.[FSP Approval Date],
			[Children in Custody]

		-- start with a base of all actively assigned caseworkers and case id's
		from (
			select distinct CASW_CASE_ID as 'Case ID',
				CASW_COUNTY as 'County Code',
				CASW_WORKER_ID as 'Worker ID',
				CASW_SUPER_ID as 'Supervisor ID'
			from MW_CASE_WORKER
			where CASW_COUNTY_TYPE = 'COR'
				and CASW_STATUS = 'A'
		) base

		-- Add HH ID
		left join (
			-- Get HH Name for most recent HH assigned per case
			select distinct casp_base.[Case ID],
				casp_base.[HH ID],
				hh_name.[HH Name]
			from (
				select ISN as 'CASP ISN',
					CASP_CASE_ID as 'Case ID',
					CASP_PERS_ID as 'HH ID'
				from MW_CASE_PERS
				where CASP_TYPE = 'HH'
					and CASP_STATUS = 'A'
					and ISNULL(CASP_RSN_CHANGE, 1) != 150
			) casp_base
			inner join (
				select MAX(ISN) 'Max CASP ISN',
					CASP_PERS_ID as 'HH ID'
				from MW_CASE_PERS
				where CASP_TYPE = 'HH'
					and CASP_STATUS = 'A'
					and ISNULL(CASP_RSN_CHANGE, 1) != 150
				group by CASP_PERS_ID
			) rec_filter
			on casp_base.[CASP ISN] = rec_filter.[Max CASP ISN]
			left join (
				select RTRIM(PERS_LAST_NAME) + ', ' + RTRIM(PERS_FIRST_NAME) as 'HH Name',
					PERS_PERS_ID as 'Person ID'
				from MW_PERS
			) hh_name
			on hh_name.[Person ID] = casp_base.[HH ID]
		) hh
		on hh.[Case ID] = base.[Case ID]

		-- Add county and region names
		left join (
			select county_code,
				RTRIM(county_name) as 'County',
				RTRIM(region_name) as 'Region'
			from COUNTY_REGION
		) coreg
		on base.[County Code] = coreg.county_code

		-- Add worker/supervisor names, filter out Aging cases
		inner join (
			select RTRIM(RTRIM(WORK_LAST_NAME) + ', ' + RTRIM(WORK_FIRST_NAME) + ' ' + ISNULL(RTRIM(WORK_INIT), '')) as 'Worker Name',
				WORK_WORKER_ID as 'Worker ID'
			from MW_WORKER
			where WORK_UNIT_TITLE != 'Aging'
		) wrkr
		on wrkr.[Worker ID] = base.[Worker ID]

		left join (
			select RTRIM(RTRIM(WORK_LAST_NAME) + ', ' + RTRIM(WORK_FIRST_NAME) + ' ' + ISNULL(RTRIM(WORK_INIT), '')) as 'ASWS Name',
				WORK_WORKER_ID as 'ASWS ID'
			from MW_WORKER
		) asws
		on asws.[ASWS ID] = base.[Supervisor ID]

		--Add Case Start Date
		left join (
			select CASE_ID as 'Case ID',
				CASE_DATE_OPENED as 'Case Start'
			from MW_CASE

			--Filter for most recent case open period
			inner join (
				select MAX(ISN) as 'Max ISN',
					CASE_ID as 'Case ID'
				from MW_CASE
				group by CASE_ID
			) max_isn
			on max_isn.[Max ISN] = ISN
		) case_start
		on case_start.[Case ID] = base.[Case ID]

		-- Get all entered services
		left join (
			select FSPL_CASE_ID as 'Case ID',
				COUNT(*) as 'Num Services'
			from MW_FSP,
				MW_DIR_SRVC
			where FSPL_FSP_ID = DIRS_FSP_ID
				and (DIRS_END_DATE is NULL
				or DIRS_END_DATE >= GETDATE())
				--and FSPL_CASE_ID = '375225'
			group by FSPL_CASE_ID
		) all_fsps
		on all_fsps.[Case ID] = base.[Case ID]

		-- Add latest FSP (Any Type) and Approval Dates
		left join (
			select fsp_base.[Case ID],
				fsp_base.[FSP Approval Date],
				plan_type.[Plan Type],
				fsp_base.[Plan Status]
			from (
				select FSPL_CASE_ID as 'Case ID',
					FSPL_PLAN_TYPE as 'Plan Type Code',
					FSPL_APPROVE_DT as 'FSP Approval Date',
					FSPL_STATUS as 'Plan Status',
					FSPL_TIMESTAMP as 'Timestamp'
				from MW_FSP
				--Filter by most recent approval date
				inner join (
					select MAX(FSPL_APPROVE_DT) as 'Max Approval Date',
						FSPL_CASE_ID as 'Case ID'
					from MW_FSP
					group by FSPL_CASE_ID
				) rec_filter
				on FSPL_APPROVE_DT = rec_filter.[Max Approval Date]
					and FSPL_CASE_ID = rec_filter.[Case ID]
				-- In cases with multiple records with the same approval date, filter also by timestamp
				inner join (
					select MAX(FSPL_TIMESTAMP) as 'Max Timestamp',
						FSPL_APPROVE_DT as 'Approval Date',
						FSPL_CASE_ID as 'Case ID'
					from MW_FSP
					group by FSPL_APPROVE_DT,
						FSPL_CASE_ID
				) mult_filter
				on FSPL_APPROVE_DT = mult_filter.[Approval Date]
					and FSPL_CASE_ID = mult_filter.[Case ID]
					and FSPL_TIMESTAMP = mult_filter.[Max Timestamp]
			) fsp_base
			left join (
				select CODE_TABLE_VALUE as 'Plan Type Code',
					RTRIM(CODE_SHORT_DESC) as 'Plan Type'
				from MW_CODE_TABLE
				where CODE_TABLE_NAME = 'FSP_APPR'
			) plan_type
			on fsp_base.[Plan Type Code] = plan_type.[Plan Type Code]
		) fsp
		on fsp.[Case ID] = base.[Case ID]

		-- Get number of custody children
		left join (
			select *
			from @custody_children_per_case
		) num_cust
		on base.[Case ID] = num_cust.[Case ID]


		where all_fsps.[Num Services] is NULL
			and [Plan Type] is NULL
	) fsp_report
	return
end
*/

select * from dbo.EWB_Cases_With_No_Services_Report()
