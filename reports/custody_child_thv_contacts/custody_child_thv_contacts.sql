select [Data Date]
	,Region
	,County
	,[ASWS Name]
	,[Worker Name]
	,[Child ID]
	,[Child Name]
	,convert(varchar, [Placement Start], 101) as 'Placement Start'
	,CONVERT(varchar, [First Monthly Contact Date], 0) as 'First Monthly Contact Date'
	,CONVERT(varchar, [Recent Monthly Contact Date], 0) as 'Recent Monthly Contact Date'
	,CONVERT(varchar, [Recent Contact], 101) as 'Recent Contact'
	,[Days Since Last Contact]
	,[Contacts This Month]
	,[Indicator Column]
from fod_worker_thv_child_visits
order by 2, 3, 4, 5, 7