select [Data Date]
	,Region
	,County
	,[ASWS Name]
	,[Worker Name]
	,[Case ID]
	,[Child ID]
	,[Child Name]
	,[Custody Start]
	,[Worker Assignment Date]
	,convert(varchar, [Recent Contact], 0) as 'Recent Contact'
	,[Recent Monthly Contact Date]
	,[Days Since Last Contact]
	,[Contacts This Month]
	,[Contacts This Month (In-Home)]
	,case
		when [Days Since Last Contact] > 25 and [Contacts This Month] = 0 then 2
		when [Days Since Last Contact] > 30 then 2
		when [Days Since Last Contact] > 20 and [Contacts This Month] = 0 then 1
		when [Days Since Last Contact] > 25 then 1
	else 0
	end as 'Indicator Column'
from fod_adop_wrkr_child_contacts
