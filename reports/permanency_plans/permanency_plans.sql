  SELECT Distinct GETDATE() as 'Data Date',
                 rtrim(region) as 'Region',
                 rtrim(county) as 'County' ,
	              rtrim(rtrim(C.super_last) + ', ' + rtrim(C.super_first) + ' ' + isnull(rtrim(C.super_init),'')) 'ASWS Name',
                rtrim(rtrim(C.worker_last) + ', ' + rtrim(C.worker_first) + ' ' + isnull(rtrim(C.worker_int),'')) 'Worker Name',
                 upper(a.pers_id) as 'Child ID',
                 upper(a.custody_child_name) as 'Custody Child Name',
                 upper(a.cust_start_date) as 'Custody Start Date',
                 upper(a.days_in_cust) as 'Days In Custody',
                 upper(e.months_in_plan) as 'Months In Plan',
                 upper(e.plan_plan_description) as 'Permanency Plan',
                 upper(f.concurrent_plan) as 'Concurrent Plan',
				 case
				  when e.plan_plan_description in ('Long Term Foster Care', 'Living Independently') then 0
				  when f.concurrent_plan in ('Long Term Foster Care', 'Living Independently') then 0
          when e.plan_plan_description = 'Another permanent planned living arrangement' and DATEDIFF(YY, g.PERS_DOB, GETDATE()) < 16 then 0
					when a.days_in_cust > 45 and e.plan_plan_description is NULL then 0
					when a.days_in_cust > 365 and e.plan_plan_description = 'REUNIFICATION WITH PRIMARY CARETAKER PARENTS' then 50
					when a.days_in_cust > 456 and e.plan_plan_description <> 'ADOPTION' then 50
					when a.days_in_cust <= 45 and e.plan_plan_description is NULL then 50
					else 100
				end as 'Indicator Column'
                 from CUSTODY_MEMBERS_PLAN a
                 join CUSTODY_WORKERS_PLAN C on a.case_id = c.case_id 
          and a.pers_id = C.pers_id
     left join CUSTODY_PLACEMENTS_PLAN d on a.pers_id = d.pers_id
          and d.placement_status = 'A'
     left join CUSTODY_PERM_PLAN_PLAN e on a.pers_id = E.pers_id  
     left join CUSTODY_CONCURRENT_PLAN f on a.pers_id = f.pers_id and e.fsp_id=f.fsp_id
     left join MW_PERS g on a.pers_id = g.PERS_PERS_ID 
                
  order by 1 asc, 2 asc, 4 asc