select GETDATE() as 'Data Date'
	,Region
	,County
	,[ASWS Name]
	,[Worker Name]
	,[Case ID]
	,[Resource ID]
	,[Foster Parent IDs]
	,[Foster Parent Names]
	,[Child IDs]
	,[Child Names]
	,CONVERT(VARCHAR, [Recent Contact], 0) as 'Recent Contact'
	,[Days Since Last Contact]
	,[Foster Parent(s) Contacted]
	,Convert(VARCHAR, [Recent Monthly Contact], 0) as 'Recent Monthly Contact'
	,[Contacts This Month]
	,case
	  when [Days Since Last Contact] > 25 and [Contacts This Month] = 0 then 2
	  when [Days Since Last Contact] > 30 then 2
	  when [Days Since Last Contact] > 20 and [Contacts This Month] = 0 then 1
	  when [Days Since Last Contact] > 25 then 1
	  else 0
	 end as 'Indicator Column'
from fod_worker_foster_par_contacts
