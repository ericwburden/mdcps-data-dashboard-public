with ar1k as (
	select Region
		,County
		,[ASWS Name]
		,[Worker Name]
		,'Direct Service' as [Unit Title]
		,[Case Weight]
	from (
	  select distinct
		GETDATE() as 'Data Date',
		cor_region as 'Region',
		service_county as  'County',
		rtrim(supervisor) as 'ASWS Name', 
		rtrim(worker) as 'Worker Name',
		catalyst_desc as 'Catalyst Description', 
		case_id as 'Case ID', 
		case_hh_name 'Case HH Name', 
		person_id 'Child ID', 
		person_name  'Child Name', 
		Custody_Start_date as 'Custody Start Date',
		Facility_Name as 'Facility Name',
		RESD_RESOURCE_NAME as 'Resource Name',
		  weights as 'Case Weight'
	  from [dbo].[ZZZAr2K_Output_Unit_Title]
	  left join MW_WORKER mw
	  on [dbo].[ZZZAr2K_Output_Unit_Title].worker_id = mw.WORK_WORKER_ID
		and mw.WORK_ACTIVE_INDICATOR = 1 
		and mw.WORK_UNIT_TITLE='DIRECT SERVICE'
	  where cor_region not in ('Special Investigation Unit','STATEWIDE')
	) ar1k
), ar3k as(
	SELECT rtrim(region) as 'Region',
		rtrim(county) as 'County',
		rtrim(rtrim(sup_last) +', '+rtrim(sup_first)+' '+isnull(rtrim(sup_middle),'')) 'ASWS Name',
		rtrim(rtrim(work_last) +', '+rtrim(work_first)+' '+isnull(rtrim(work_middle),'')) 'Worker Name',
		case
			when Upper(unit_title) = 'RESOURCE' then 'Resource'
			else 'Special Investigations'
		end as 'Unit Title',
		Upper(weights) as 'Case Weight'
	from  AR3K_OUTPUT
)
	
select GETDATE() as 'Data Date'
	,Region
	,County
	,[ASWS Name]
	,[Worker Name]
	,[Unit Title]
	,sum([Case Weight]) as 'Caseload'
from (
	select * from ar1k 
	union all
	select * from ar3k 
) all_workload
group by Region
	,County
	,[ASWS Name]
	,[Worker Name]
	,[Unit Title]
