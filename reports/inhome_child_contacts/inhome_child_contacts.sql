select fwicc.*,
	CASE WHEN (
		[Days Since Last Visit] > 25
		or (DAY(GETDATE()) > 20 and [Contacts This Month] = 0)
		or (DAY(GETDATE()) > 25 and ([Contacts This Month (In Home)] = 0 or [Contacts This Month (Private)] = 0))
		or (DAY(GETDATE()) > 25 and [Contacts This Month] = 1)
	) THEN 2
	ELSE CASE WHEN (
		[Days Since Last Visit] > 15
		or (DAY(GETDATE()) > 15 and [Contacts This Month] = 0)
		or (DAY(GETDATE()) > 20 and ([Contacts This Month (In Home)] = 0 or [Contacts This Month (Private)] = 0))
		or (DAY(GETDATE()) > 20 and [Contacts This Month] = 1)
	) THEN 1
	ELSE 0
	END END AS 'Indicator Column'
from fod_worker_inhome_child_contacts fwicc