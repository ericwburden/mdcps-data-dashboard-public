with query as (
	SELECT Distinct GETDATE() as 'Data Date',
					 rtrim(region) as 'Region',
					 rtrim(county) as 'County' ,
					  rtrim(asws) as 'ASWS Name',
					rtrim(Worker) as 'Worker Name',
					 upper(child_id) as 'Child ID',
					  upper(child_name) as 'Child Name',
					  upper(cust_start_date) as 'Custody Start Date',
					  upper(shlt_due_date) as 'Shelter Due Date',
					 upper(shlt_hearing_date) as 'Shelter hearing date',
					 upper(adju_due_date) as 'Adjudication Due Date',
					 upper(adju_hearing_date) as 'Adjudication hearing date',
					 upper(displ_due_date) as 'Dispositional Due Date',
					 upper(displ_hearing_date) as 'Dispositional hearing date',
					 upper(perm_due)  as 'Permanency Due Date'  ,
					  upper(perm_hearing_date) as 'Permanency hearing date',
					  DUE_TYPE as 'Next Hearing Due',
	case
						when DATEDIFF(D,cust_start_date, GETDATE()) > 30 and adju_hearing_date is NULL then 3
						when DATEDIFF(D,cust_start_date, GETDATE()) > 44 and displ_hearing_date is NULL then 3
						when DATEDIFF(D,cust_start_date, GETDATE()) > 365 and perm_hearing_date is NULL then 3
						when DATEDIFF(D,cust_start_date, GETDATE()) <= 2 and shlt_hearing_date is NULL then 2
						when DATEDIFF(D,cust_start_date, GETDATE()) <= 29 and adju_hearing_date  is NULL  then 2
						when (DATEDIFF(D,cust_start_date, GETDATE()) between 30 and 44) and displ_hearing_date is NULL then 2
						when DATEDIFF(D, GETDATE(), perm_due) < 60 then 2
						else 0
			   end as 'Indicator Column',
					  --due dates 
					  case 
			   when DUE_TYPE='Shelter hearing'
			   then shlt_due_date
				when DUE_TYPE='Adjudication hearing'
			   then adju_due_date
			   when DUE_TYPE='Dispositional hearing'
			   then displ_due_date
			   when DUE_TYPE='Permanency hearing'
			   then perm_due
				end as 'Next hearing due date',
				--for no of days 
				case 
					when DUE_TYPE='Shelter hearing' then DATEDIFF(dd,GETDATE(),shlt_due_date)
					when DUE_TYPE='Adjudication hearing' then DATEDIFF(dd,GETDATE(),adju_due_date)
					when DUE_TYPE='Dispositional hearing'   then DATEDIFF(dd,GETDATE(),displ_due_date)
					when DUE_TYPE='Permanency hearing' then DATEDIFF(dd,GETDATE(),perm_due)
					--when DUE_TYPE='Dispositional hearing' then DATEDIFF(dd,GETDATE(),isnull(perm_due,getdate()))--added 
				end as 'Days Until Next Hearing Due'
	from court_due_table
)

select [Data Date]
	,Region
	,County
	,[ASWS Name]
	,[Worker Name]
	,[Child ID]
	,[Child Name]
	,[Custody Start Date]
	,[Shelter Due Date]
	,[Shelter hearing date]
	,[Adjudication Due Date]
	,[Adjudication hearing date]
	,[Dispositional Due Date]
	,[Dispositional hearing date]
	,[Permanency Due Date]
	,[Permanency hearing date]
	,[Next Hearing Due]
	,[Next hearing due date]
	,case
		when [Days Until Next Hearing Due] < 0 then 0
		else [Days Until Next Hearing Due]
	end as 'Days Until Next Hearing Due'
	,[Indicator Column]
from query     

order by  Region,County,[ASWS Name],[Worker Name]
