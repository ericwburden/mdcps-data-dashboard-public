select [Data Date]
	,Region
	,County
	,[ASWS Name]
	,[Worker Name]
	,[Intake ID]
	,[Named Perpetrators]
	,[Named Victims]
	,convert(varchar, [Initiation DateTime], 0) as 'Initiation DateTime'
	,[Report Level]
	,[Hours to Initiate]
	,[Initiated Timely]
	,[Days Open]
from fod_open_investigations
where [Worker Unit Title] = 'Direct Service'
