SELECT DISTINCT GETDATE() as 'Data Date',
	RTRIM(casw.WORK_REGION) AS Region, 
	RTRIM(casw.WORK_CO_NAME) AS County, 
	RTRIM(RTRIM(asws.WORK_LAST_NAME) + ', ' + RTRIM(asws.WORK_FIRST_NAME) + ' ' + ISNULL(RTRIM(asws.WORK_INIT), '')) AS [ASWS Name], 
	RTRIM(RTRIM(wrkr.WORK_LAST_NAME) + ', ' + RTRIM(wrkr.WORK_FIRST_NAME) + ' ' + ISNULL(RTRIM(wrkr.WORK_INIT), '')) AS [Worker Name],
	CONVERT(VARCHAR, narr.NARR_DATETIME, 0) as 'Narrative Time Recorded',
	CONVERT(VARCHAR, narr.NARR_TIMESTAMP, 0) as 'Narrative Time Entered',
	narr.ENTRY_LAG as 'Days to Enter Narrative',
	narr.CONTACT_TYPE as 'Contact Type',
	narr.CONTACT_METHOD as 'Contact Method',
	narr.CONTACT_LOC as 'Contact Location',
	narr.PARTICIPANT as 'Participant'

-- Start with a base of all caseworker assignments
FROM (
	select awbt.*,
		wrkr.WORK_CO_NAME,
		wrkr.WORK_REGION
	from EWB_All_Workers_By_Type_Dev awbt
	left join MW_WORKER wrkr
	on wrkr.WORK_WORKER_ID = awbt.[Worker ID]
	where awbt.[Work Type] = 'Direct Service'
) AS casw 

-- Add worker name and filter out Aging and Adult Services workers
INNER JOIN (
	SELECT WORK_WORKER_ID, WORK_LAST_NAME, WORK_FIRST_NAME, WORK_INIT
	FROM dbo.MW_WORKER
	where WORK_UNIT_TITLE != 'Aging'
) AS wrkr 
ON casw.[Worker ID] = wrkr.WORK_WORKER_ID 

-- Add supervisor name
INNER JOIN (
	SELECT WORK_WORKER_ID, WORK_LAST_NAME, WORK_FIRST_NAME, WORK_INIT
	FROM dbo.MW_WORKER AS MW_WORKER_1
) AS asws 
ON casw.[ASWS ID] = asws.WORK_WORKER_ID

-- Add narrative information
LEFT JOIN (
	SELECT NARR_OWNER,
	NARR_DATETIME,
	NARR_TIMESTAMP,
	DATEDIFF(DD, NARR_DATETIME, NARR_TIMESTAMP) as ENTRY_LAG,
	ISNULL(
		cf_typ_info.CODE_SHORT_DESC,
		ISNULL(
			hc_typ_info.CODE_SHORT_DESC,
			ISNULL(
				hh_typ_info.CODE_SHORT_DESC,
				ISNULL(
					ih_typ_info.CODE_SHORT_DESC,
					ISNULL(
						ii_typ_info.CODE_SHORT_DESC,
						ISNULL(
							iq_typ_info.CODE_SHORT_DESC,
							rr_typ_info.CODE_SHORT_DESC
						)
					)
				)
			)
		)
	) as CONTACT_TYPE,
	mthd_info.CODE_SHORT_DESC as CONTACT_METHOD,
	loc_info.CODE_SHORT_DESC as CONTACT_LOC,
	napr.PARTICIPANT

	-- Start with MW_NARRATIVE information
	from( 
		select NARR_DATETIME,
			NARR_TIMESTAMP,
			NARR_SYSTEM_TYPE,
			NARR_SUB_SYSTEM_TYPE,
			NARR_OWNER,
			NARR_CONTACT_METHOD,
			NARR_CONTACT_TYPE,
			NARR_LOCATION,
			NARR_NARRATIVE_ID
		FROM MW_NARRATIVE
		WHERE NARR_DATETIME >= DATEADD(DD, -15, GETDATE())
		  and NARR_DATETIME is not NULL
	) narr

	--Replace codes with values
	left join (
		select CODE_TABLE_NAME, CODE_TABLE_VALUE, CODE_SHORT_DESC
		from MW_CODE_TABLE
		where CODE_TABLE_NAME = 'CNTCTMTD'
	) mthd_info
	on narr.NARR_CONTACT_METHOD = mthd_info.CODE_TABLE_VALUE
	left join (
		select CODE_TABLE_NAME, CODE_TABLE_VALUE, CODE_SHORT_DESC
		from MW_CODE_TABLE
		where CODE_TABLE_NAME = 'NARRFOCH'
	) cf_typ_info
	on narr.NARR_CONTACT_TYPE = cf_typ_info.CODE_TABLE_VALUE
		and narr.NARR_SYSTEM_TYPE = 'C' and narr.NARR_SUB_SYSTEM_TYPE = 'F'
	left join (
		select CODE_TABLE_NAME, CODE_TABLE_VALUE, CODE_SHORT_DESC
		from MW_CODE_TABLE
		where CODE_TABLE_NAME = 'NARRFOHF'
	) hc_typ_info
	on narr.NARR_CONTACT_TYPE = hc_typ_info.CODE_TABLE_VALUE
		and narr.NARR_SYSTEM_TYPE = 'H' and narr.NARR_SUB_SYSTEM_TYPE = 'C'
	left join (
		select CODE_TABLE_NAME, CODE_TABLE_VALUE, CODE_SHORT_DESC
		from MW_CODE_TABLE
		where CODE_TABLE_NAME = 'NARRFOHF'
	) hh_typ_info
	on narr.NARR_CONTACT_TYPE = hh_typ_info.CODE_TABLE_VALUE
		and narr.NARR_SYSTEM_TYPE = 'H' and narr.NARR_SUB_SYSTEM_TYPE = 'H'
	left join (
		select CODE_TABLE_NAME, CODE_TABLE_VALUE, CODE_SHORT_DESC
		from MW_CODE_TABLE
		where CODE_TABLE_NAME = 'NARRINVS'
	) ii_typ_info
	on narr.NARR_CONTACT_TYPE = ii_typ_info.CODE_TABLE_VALUE
		and narr.NARR_SYSTEM_TYPE = 'I' and narr.NARR_SUB_SYSTEM_TYPE = 'I'
	left join (
		select CODE_TABLE_NAME, CODE_TABLE_VALUE, CODE_SHORT_DESC
		from MW_CODE_TABLE
		where CODE_TABLE_NAME = 'NARRRESI'
	) ih_typ_info
	on narr.NARR_CONTACT_TYPE = ih_typ_info.CODE_TABLE_VALUE
		and narr.NARR_SYSTEM_TYPE = 'I' and narr.NARR_SUB_SYSTEM_TYPE = 'H'
	left join (
		select CODE_TABLE_NAME, CODE_TABLE_VALUE, CODE_SHORT_DESC
		from MW_CODE_TABLE
		where CODE_TABLE_NAME = 'NARRRESI'
	) iq_typ_info
	on narr.NARR_CONTACT_TYPE = iq_typ_info.CODE_TABLE_VALUE
		and narr.NARR_SYSTEM_TYPE = 'I' and narr.NARR_SUB_SYSTEM_TYPE = 'Q'
	left join (
		select CODE_TABLE_NAME, CODE_TABLE_VALUE, CODE_SHORT_DESC
		from MW_CODE_TABLE
		where CODE_TABLE_NAME = 'NARRRFHS'
	) rr_typ_info
	on narr.NARR_CONTACT_TYPE = iq_typ_info.CODE_TABLE_VALUE
		and narr.NARR_SYSTEM_TYPE = 'R' and narr.NARR_SUB_SYSTEM_TYPE = 'R'
	left join (
		select CODE_TABLE_NAME, CODE_TABLE_VALUE, CODE_SHORT_DESC
		from MW_CODE_TABLE
		where CODE_TABLE_NAME = 'CNTCTLOC'
	) loc_info
	on narr.NARR_LOCATION = loc_info.CODE_TABLE_VALUE

	--Add participants
	left join(
		select ISNULL(
				pers.PERS_NAME,
				wrkr.WRK_NAME
			) as PARTICIPANT,
			NAPR_NARRATIVE_ID
		from MW_NARR_PART_REL
		left join (
			select WORK_WORKER_ID,
				RTRIM(WORK_LAST_NAME) + ', ' + RTRIM(WORK_FIRST_NAME) as WRK_NAME
			from MW_WORKER
		) wrkr
		on NAPR_PARTICIPANT = wrkr.WORK_WORKER_ID
			and NAPR_PART_TYPE = 'W'
		left join (
			select PERS_PERS_ID,
				RTRIM(PERS_LAST_NAME) + ', ' + RTRIM(PERS_FIRST_NAME) as PERS_NAME
			from MW_PERS
		) pers
		on NAPR_PARTICIPANT = pers.PERS_PERS_ID
			and NAPR_PART_TYPE = 'P'
	) napr
	on narr.NARR_NARRATIVE_ID = NAPR_NARRATIVE_ID
) AS narr
on casw.[Worker ID] = narr.NARR_OWNER
