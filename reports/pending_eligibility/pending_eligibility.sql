select GETDATE() as 'Data Date',
  RTRIM(co_reg.region_name) as 'Region',
  RTRIM(co_reg.county_name) as 'County',
  asws.asws_name as 'ASWS Name',
  wrkr.worker_name as 'Worker Name',
  child.child_name as 'Child Name',
  DATEDIFF(DD, cust.CUST_IN_CUSTODY_DT, GETDATE()) as 'Days in Custody'
from ( 
  select * 
  from MW_CUSTODY_PER 
  where CUST_IN_CUSTODY_DT <= GETDATE()
    and CUST_CUSTODY_RELEASE_DT is NULL
) cust
inner join (
  select CASP_PERS_ID, CASP_CASE_ID
  from MW_CASE_PERS
  where CASP_END_DATE is NULL
) casp
on casp.CASP_PERS_ID = cust.CUST_CHILD_PERS_ID
left join (
  select casw_base.ISN,
    casw_base.CASW_CASE_ID, 
    casw_base.CASW_COUNTY, 
    casw_base.CASW_COUNTY_TYPE, 
    casw_base.CASW_SUPER_ID, 
    casw_base.CASW_WORKER_ID,
    casw_base.CASW_START_DATE
  from MW_CASE_WORKER casw_base
  inner join (
    select max(ISN) ISN, CASW_CASE_ID, CASW_COUNTY, CASW_COUNTY_TYPE
    from MW_CASE_WORKER
    where CASW_END_DATE is NULL
    group by CASW_CASE_ID, CASW_COUNTY, CASW_COUNTY_TYPE
  ) filter
  on casw_base.ISN = filter.ISN
  where casw_base.CASW_COUNTY_TYPE = 'COR'
) casw
on casw.CASW_CASE_ID = casp.CASP_CASE_ID
left join (
  select PERS_PERS_ID, 
  RTRIM(PERS_LAST_NAME) + ', ' + RTRIM(PERS_FIRST_NAME) child_name
  from MW_PERS
) child
on cust.CUST_CHILD_PERS_ID = child.PERS_PERS_ID
left join (
  select WORK_WORKER_ID, 
    RTRIM(RTRIM(WORK_LAST_NAME) + ', ' + RTRIM(WORK_FIRST_NAME) + ' ' + ISNULL(RTRIM(WORK_INIT), '')) worker_name
  from MW_WORKER
) wrkr
on casw.CASW_WORKER_ID = wrkr.WORK_WORKER_ID
left join (
  select WORK_WORKER_ID, 
    RTRIM(RTRIM(WORK_LAST_NAME) + ', ' + RTRIM(WORK_FIRST_NAME) + ' ' + ISNULL(RTRIM(WORK_INIT), '')) asws_name
  from MW_WORKER
) asws
on casw.CASW_SUPER_ID = asws.WORK_WORKER_ID
left join (
  select * from COUNTY_REGION
) co_reg
on co_reg.county_code = casw.CASW_COUNTY
left join (
  select base.* from MW_IVE_ELIG base
  inner join (
    select max(ISN) ISN, IVEE_PERS_ID
    from MW_IVE_ELIG
    group by IVEE_PERS_ID
  ) filter
  on base.ISN = filter.ISN
) ivee
on ivee.IVEE_PERS_ID = cust.CUST_CHILD_PERS_ID
where ivee.IVEE_ELIG_TYPE is NULL
