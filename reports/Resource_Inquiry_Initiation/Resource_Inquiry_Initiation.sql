
		
		select GETDATE() as 'Data Date',
       rtrim(region) as 'Region',
	   rtrim(county) as 'County',
	   ASWS as 'ASWS Name',
	   Worker as 'Worker Name',
	   res_id as 'Resource ID',
	   res_pers_id as 'Resource HH ID',
	   ltrim(rtrim(res_HH_lname))+','+ltrim(rtrim(res_HH_fname))  as 'Resource HH Name',
	   res_in_dt as 'Resource Inquiry Date',
	   int_packet_dt as 'Packet Sent Date',
	   train_dt as 'Training Completed Date',
	   info_pro_dt as 'Info Provided Date',
	   no_of_days as 'No of Days to 1st Action',
	   case
						when no_of_days is NULL then 1
						else 0
					end as 'Without Action'
					
	   from RES_INQUIRY_INITIATIVE order by 2,3,4,5