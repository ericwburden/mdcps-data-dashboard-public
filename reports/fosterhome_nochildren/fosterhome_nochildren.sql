 select Distinct GETDATE() as 'Data Date',
     a.region as 'Region',
     a.county as 'County',
	  ltrim(rtrim(a.supr_last))+','+ltrim(rtrim(a.supr_first))+' ' +isnull(rtrim(a.supr_init),' ') 'ASWS Name',
	ltrim(rtrim(a.work_last))+','+ltrim(rtrim(a.work_first))+' '+isnull(rtrim(a.work_init),' ') 'Worker Name', 
      a.res_name 'Resource Name', 
		a.res_id 'Resource ID',
		a.res_status'Resource Status',
		a.lic_st_dt ' Initial License Start Date',
					Case
		    when g.RESS_SERVICE_TYPE_CD  = 9020
		    then 'Foster Home'
		    when g.RESS_SERVICE_TYPE_CD  = 9040
		    then 'Emergency Foster Home'
		    when g.RESS_SERVICE_TYPE_CD  = 9060
		    then 'Therapeutic Foster Home'
		    when g.RESS_SERVICE_TYPE_CD  = 9080
		    then 'Foster/Adopt Foster Home'
		    when g.RESS_SERVICE_TYPE_CD  = 9110
		    then 'Adoption Unit Foster Home'
		    when g.RESS_SERVICE_TYPE_CD  = 9130
		    then 'Adoptive Home (Domestic)'
		    when g.RESS_SERVICE_TYPE_CD  = 9150
		    then 'Respite Foster Home'
		    when g.RESS_SERVICE_TYPE_CD  = 9170
		    then 'Medical/Treatment Foster Home'
		    when g.RESS_SERVICE_TYPE_CD  = 9190
		    then 'Relative Foster Home'
		    when g.RESS_SERVICE_TYPE_CD  = 9210
		    then 'Teenage Parent Foster Home'
		    when g.RESS_SERVICE_TYPE_CD = 9275
		    then 'ICPC-Incoming'
			 when g.RESS_SERVICE_TYPE_CD  = 9280
		    then 'ICPC-Outgoing'
			 when g.RESS_SERVICE_TYPE_CD  = 9290
		    then 'Child-Specific'
		    else g.RESS_SERVICE_TYPE_CD 
		 end as  'Service Type',
		  DATEDIFF(D,e.plac_end_dt, getdate()) as 'Days since child placed' ,
		   case
		  when DATEDIFF(D,e.plac_end_dt, getdate()) <= 90 then 1
		  else 2
		  end as 'Indicator column'
					from fosterhome_table a
	left join  placements_foster e on 
	a.res_id = e.res_id and e.PLAC_STATUS = 'C'
	and e.plac_status !='A'
	left join  MW_RESRC_DIR f on 
	e.res_id = f.RESD_RESOURCE_ID  
	left join MW_RES_SERVICES g 
	on g.RESS_RESOURCE_ID =e.res_id
	where (RESS_START_DT is null or RESS_START_DT <= GETDATE())
		and (RESS_END_DT is null or RESS_END_DT >= getdate())
and g.RESS_RESOURCE_ID = a.res_id 