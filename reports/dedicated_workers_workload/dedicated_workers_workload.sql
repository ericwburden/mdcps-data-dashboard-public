select [Data Date],
	Region,
	County,
	[ASWS Name],
	[Worker Name],
	[Service Type],
	[Case ID] as 'Unique ID',
	[Intake HH ID],
	case
		when [Intake HH Name] is not NULL then [Intake HH Name]
		when [Case First Name] is NULL then [Case Last Name]
		else RTRIM([Case Last Name]) + ', ' + RTRIM([Case First Name])
	end as 'Case Name',
	Weights as 'Case Weight'
from (
	SELECT GETDATE() as 'Data Date',
		rtrim(region) as 'Region',
		rtrim(county) as 'County',
		rtrim(rtrim(sup_last) +', '+rtrim(sup_first)+' '+isnull(rtrim(sup_middle),'')) 'ASWS Name',
		rtrim(rtrim(work_last) +', '+rtrim(work_first)+' '+isnull(rtrim(work_middle),'')) 'Worker Name',
		Upper(unit_title) as 'Unit Title',
		Upper(serv_type) as 'Service Type',
		Upper(case_id) as 'Case ID',
		Upper(case_last) as 'Case Last Name',
		Upper(case_first) as 'Case First Name',
		Upper(intake_dt) as 'Intake Date',
		Upper(intake_hh_id) as 'Intake HH ID',
		Upper(intake_hh_name) as 'Intake HH Name',
		Upper(weights) as 'Weights'
	from  AR3K_OUTPUT
	where UPPER(unit_title) = 'RESOURCE'
) query
order by 2 asc, 3 asc, 5 asc
