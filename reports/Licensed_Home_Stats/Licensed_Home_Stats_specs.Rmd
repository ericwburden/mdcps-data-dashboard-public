---
title: "LICENSED HOME STATS REPORT"
output: word_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE)
output_type <- knitr::opts_knit$get('rmarkdown.pandoc.to')
```

### Row Colors:
       
 - Red: This item is either overdue or will be soon, needs urgent attention. A record is given this designation if any of the following criteria is met:
       - The number of children in placement is over 5.
       - The number of children in placement under 3years is over 2.
       - The total number of Approved Slots is over 5.
  
    
### Business Rules

- The report pulls all resource homes that are currently licensed.
- The Region, County, Resource ID, Resource Name, Service Type, number of Approved Slots,number of children in placement and number of children in placement under 3 years of age all come from the Resource Directory.
- The Worker comes from the active RSP worker record on the Resource Service Plan/RSP History tab.
- The ASWS comes from the Worker's immediate Supervisor in Personnel.
- The Service Type lists all services currently provided by the home and comes from the Resource Directory.

### Note

- If the home has more than one Service Type it will be listed on the report with a row for each type, but the 
    numbers for the Approved Slots will remain the same.
   
### Hidden Columns

- Indicator Column (Determines color of row)
- Resourc ID
- Approved Slots Boys
- Approved Slots Girls
- Approved Slots Either