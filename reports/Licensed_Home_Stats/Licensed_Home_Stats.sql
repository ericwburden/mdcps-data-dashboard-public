select distinct 
       GETDATE() as 'Data Date',
       a.region as 'Region',
	   a.county as 'County',
	   ltrim(rtrim(a.super_last)) + ', ' + ltrim(rtrim(a.super_first ))+ ' ' + isnull(rtrim(a.super_middle),' ') as 'ASWS Name',
       ltrim(rtrim(a.worker_last )) + ', ' + ltrim(rtrim(a.worker_first ))+ ' ' + isnull(rtrim(a.worker_middle),' ') as 'Worker Name',
       a.res_id as 'Resource ID',
       upper(a.res_name) as 'Resource Name',
		Case
		    when a.facility_type = 9000
		    then 'Placement Services'
		    when a.facility_type = 9010
		    then 'Acute Care'
		    when a.facility_type = 9020
		    then 'Foster Home'
		    when a.facility_type = 9030
		    then 'Residential Treatment'
		    when a.facility_type = 9040
		    then 'Emergency Foster Home'
		    when a.facility_type = 9050
		    then 'ICFMR'
		    when a.facility_type = 9060
		    then 'Therapeutic Foster Home'
		    when a.facility_type = 9080
		    then 'Foster/Adopt Foster Home'
		    when a.facility_type = 9130
		    then 'Adoptive Home (Domestic)'
		    when a.facility_type = 9150
		    then 'Respite Foster Home'
		    when a.facility_type = 9170
		    then 'Medical/Treatment Foster Home'
		    when a.facility_type = 9190
		    then 'Relative Foster Home'
		    when a.facility_type = 9210
		    then 'Teenage Parent Foster Home'
			 when a.facility_type = 9275
		    then 'ICPC-Incoming'
			 when a.facility_type = 9280
		    then 'ICPC-Outgoing'
		    when a.facility_type = 9410
		    then 'Expedited Pending Relative'
		    else a.facility_type
		 end as  'Service Type',
		 isnull(f.RESD_APP_BOYS_NMBR,0) as 'Approved Slots Boys',
		 isnull(f.RESD_APP_GIRLS_NMBR,0) as 'Approved Slots Girls',
		 isnull(f.RESD_APP_EITHER_NMBR,0) as 'Approved Slots Either',
		 isnull(f.RESD_APP_BOYS_NMBR,0)+isnull(f.RESD_APP_GIRLS_NMBR,0)+isnull(f.RESD_APP_EITHER_NMBR,0) as 'Approved Slots Total',
		 a.child_in_plac as 'No of Children in Placement',
		 a.child_in_plac_3 as 'No of Children in Placement under 3years'
from LICHOME_DETAIL a,
     MW_RESRC_DIR f
where
       a.res_id = f.RESD_RESOURCE_ID
	   order by region,county,[ASWS Name],[Worker Name],[Resource ID],[Resource Name]