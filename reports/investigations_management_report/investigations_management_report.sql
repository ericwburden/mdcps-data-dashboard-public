select [Data Date]
	,Region
	,County
	,[ASWS Name]
	,[Worker Name]
	,[Period Name]
	,CONVERT(VARCHAR, [Period Start], 0) as 'Period Start'
	,[Intake ID]
	,[Report Level]
	,[Victim IDs]
	,[Victim Names]
	,Placements
	,[MIC Indicated]
	,[MIC Indicated Value]
	,CONVERT(VARCHAR, [Intake DateTime], 0) as 'Intake DateTime'
	,CONVERT(VARCHAR, [Initiation DateTime], 0) as 'Initiation DateTime'
	,convert(varchar, [Findings Approved Date], 101) as 'Findings Approved Date'
	,Substantiated
	,[Substantiated Value]
	,[Hours to Initiate]
	,[Initiated Timely]
	,[Days Open]
	,[Approved Timely]
	,case
		when [Approved Timely] = 'No' then 2
		when [Initiated Timely] = 'No' then 1
		else 0
	end as 'Indicator Column'
from fod_investigations_management_report