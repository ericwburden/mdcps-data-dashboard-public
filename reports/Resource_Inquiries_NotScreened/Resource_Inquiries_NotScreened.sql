select GETDATE() as 'Data Date',
       rtrim(region) as 'Region',
	   rtrim(county) as 'County',
	   ASWS as 'ASWS Name',
	   Worker as 'Worker Name',
	   intake_id as 'Resource ID',
	   res_HH as 'Resource HH ID',
	   res_HH_name  as 'Resource HH Name',
	   res_intake_dt as 'Resource Inquiry Date',
	   scr_app_dt as ' Worker Screening Date',
	   No_Of_Days_without_Screening as 'No of Days Without Screening'
	   from RESINQ_NOTSCREENED order by region,county,ASWS,Worker
			 