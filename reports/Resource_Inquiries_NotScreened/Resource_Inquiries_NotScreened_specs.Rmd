---
title: "RESOURCE INQUIRIES NOT SCREENED REPORT"
output:
  word_document: default
  html_document: default
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE)
output_type <- knitr::opts_knit$get('rmarkdown.pandoc.to')
```

### Row Colors:

 - Yellow: Warning, this item needs attention to ensure excellent practice. A record is given this designation if the following criterion is met:
       - The number of days without screening is between 2 and 4 days.
       
 - Red: This item is either overdue or will be soon, needs urgent attention. A record is given this designation if the following criterion is met:
       - The number of days without screening is over 5 days.
  
    
### Business Rules

- The report pulls all open Resource Inquiries that have not been screened.  This includes:
       - Resource Inquiries that have been started by a Worker, but not submitted for screening.
       - Resource Inquiries that have been submitted for screening, but have not been screened by the Supervisor.
-	The Region and County are pulled from the Intake Report for the Resource.
-	The Worker is the currently assigned worker on the Resource Inquiry.
-	The ASWS is the worker's immediate supervisor in Personnel.
-	The Resource Head of Household ID and Name are from the person selected as Primary Caretaker on the Intake Report/Person Detail Info tab and the person's demographics.
-	The Resource Inquiry Date is the Report Date on the General tab of the Intake Report.
-	The Worker Screening Date is the Recommendation date on the Screening /Intake Worker tab.
-	The Number of Days without Screening is calculated from the Report Date to the data date.

   
### Hidden Columns
- Resourc ID
- Resource HH ID