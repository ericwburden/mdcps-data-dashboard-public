SELECT  distinct 
	GETDATE() as 'Data Date',
	rtrim(region) as 'Region',
	rtrim(county) as 'County' ,              
	RTRIM(RTRIM(sup_last) + ', ' + RTRIM(sup_first) + ' ' + ISNULL(RTRIM(sup_middle), '')) as 'ASWS Name',
    RTRIM(RTRIM(work_last) + ', ' + RTRIM(work_first) + ' ' + ISNULL(RTRIM(work_middle), '')) as 'Worker Name',
	child_id as 'Child ID',
	(ltrim(rtrim(child_last)) + ', ' + ltrim(rtrim(child_first))) as ' Child Name',
	plac_st_dt as 'Placement Start Date',
	plac_days as 'Days in Placement',
	upper(plac_res_name) as 'Resource Name',
	rtrim(resource_county) as 'Resource County',
	plac_type as 'Placement Type',
	res_in_dt as 'Resource Inquiry Date',
	case 
	 when plac_type = 'Expedited Pending Relative Res'AND  ASSH_SUPER_REC in ( 'A','C') THEN 'Y' else 'N'
 end 'Resource Approved'
FROM SLS319_RES_BLENDER

