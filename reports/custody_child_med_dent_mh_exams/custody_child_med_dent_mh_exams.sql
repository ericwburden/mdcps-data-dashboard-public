/*
alter function dbo.EWB_Custody_Child_Medical_Dental_MH_Exams_Report()
returns @cc_med_dent_mh_ex_rpt table (
	[Data Date] datetime,
	Region varchar(30),
	County varchar(30),
	[ASWS Name] varchar(100),
	[Worker Name] varchar(100),
	[Child Name] varchar(50),
	[Custody Start] date,
	[Recent Medical Exam Date] varchar(10),
	[Days Until Next Medical Exam Due] int,
	[Recent Dental Exam Date] varchar(10),
	[Days Until Next Dental Exam Due] int,
	[Recent MH Assessment Date] varchar(10),
	[Days Until MH Assessment Due] int,
	[Indicator Column] int
)
as
begin
	-- Table Variable Definitions
	declare @exams_in_custody_episodes table (
		Region varchar(30),
		County varchar(30),
		[Case ID] int,
		[Child ID] int,
		[Child Name] varchar(50),
		[Custody Start] date,
		[Custody End] date,
		[Exam Sequence] int,
		[Exam Date] date,
		[Exam Type Code] int,
		[Exam Type] varchar(30),
		[Third Birthday] date,
		[Fourth Birthday] date
	);
	declare @all_workers table(
		[ASWS ID] int,
		[ASWS Name] varchar(100),
		[Worker ID] int,
		[Worker Name] varchar(100)
	);
	declare @workers_per_case table(
		[Case ID] int,
		[ASWS Name] varchar(100),
		[Worker Name] varchar(100)
	);

	--------------------------------------
	------ Populate table variables ------
	--------------------------------------


	insert into @exams_in_custody_episodes
	select convert(varchar, Region) as 'Region',
		convert(varchar, County) as 'County',
		[Case ID],
		[Child ID],
		[Child Name],
		[Custody Start],
		[Custody End],
		ROW_NUMBER() over (partition by [Child ID] order by [Exam Date], exams.[Exam Type Code] desc) as 'Exam Sequence',
		[Exam Date],
		exams.[Exam Type Code],
		code_conv.[Exam Type],
		[Third Birthday],
		[Fourth Birthday]
	from dbo.Custody_Case_Crosswalk_Fun(GETDATE(), GETDATE())

	left join (
		select HEAL_PERS_ID as 'Person ID',
			HEAL_EXAM_DATE as 'Exam Date',
			HEAL_EXAM_TYPE as 'Exam Type Code'
		from MW_HEALTH_REC
		where HEAL_EXAM_TYPE in (
			100,	-- Dental
			200,	-- EPSDT 
			300,	-- Medical Exam
			400,	-- Psychiatric
			500,	-- Psychological
			700,	-- Initial Health Screening
			900		-- Mental Health Assessment
		)
	) exams
	on exams.[Person ID] = [Child ID]
		and exams.[Exam Date] between 
			[Custody Start] 
			and 
			ISNULL([Custody End], GETDATE())

	left join (
		select CODE_TABLE_VALUE as 'Exam Type Code',
			rtrim(CODE_SHORT_DESC) as 'Exam Type'
		from MW_CODE_TABLE
		where CODE_TABLE_NAME = 'EXAMTYPE'
	) code_conv
	on code_conv.[Exam Type Code] = exams.[Exam Type Code]

	-- Add in each child's third birthday
	left join (
		select PERS_PERS_ID as 'Person ID',
			DATEADD(YY, 3, PERS_DOB) as 'Third Birthday'
		from MW_PERS
	) third_birthday
	on third_birthday.[Person ID] = [Child ID]

	-- Add in each child's fourth birthday
	left join (
		select PERS_PERS_ID as 'Person ID',
			DATEADD(YY, 4, PERS_DOB) as 'Fourth Birthday'
		from MW_PERS
	) fourth_birthday
	on fourth_birthday.[Person ID] = [Child ID]

	--------------------------------------

	insert into @all_workers
	select distinct [ASWS ID],
		[ASWS Name],
		[Worker ID],
		[Worker Name]
	from EWB_All_Workers_By_Type_Dev

	--------------------------------------

	insert into @workers_per_case
	select [Case ID],
		[ASWS Name],
		[Worker Name]
	from (
		select distinct CASW_CASE_ID as 'Case ID',
			CASW_WORKER_ID as 'Worker ID',
			CASW_SUPER_ID as 'ASWS ID'
		from MW_CASE_WORKER
		where CASW_COUNTY_TYPE = 'COR'
			and CASW_STATUS = 'A'
	) worker_ids
	left join (
		select distinct [Worker ID],
			[Worker Name]
		from @all_workers
	) worker_info
	on worker_ids.[Worker ID] = worker_info.[Worker ID]
	left join (
		select distinct [ASWS ID],
			[ASWS Name]
		from @all_workers
	) asws_info
	on asws_info.[ASWS ID] = worker_ids.[ASWS ID]

	--------------------------------------

	insert into @cc_med_dent_mh_ex_rpt
	select [Data Date],
		Region,
		County,
		[ASWS Name],
		[Worker Name],
		[Child Name],
		[Custody Start],
		[Recent Medical Exam Date],
		[Days Until Next Medical Exam Due],
		[Recent Dental Exam Date],
		[Days Until Next Dental Exam Due],
		[Recent MH Assessment Date],
		[Days Until MH Assessment Due],
		CASE
			WHEN [Days Until Next Medical Exam Due] <= 5 OR [Days Until Next Dental Exam Due] <= 5 OR [Days Until MH Assessment Due] <= 5 THEN 2
			WHEN [Days Until Next Medical Exam Due] <= 30 OR [Days Until Next Dental Exam Due] <= 30 OR [Days Until MH Assessment Due] <= 30 THEN 1
			ELSE 0
		END AS 'Indicator Column'
	from (
		select GETDATE() as 'Data Date',
			Region,
			County,
			[ASWS Name],
			[Worker Name],
			base.[Child ID],
			[Child Name],
			[Custody Start],
			[Exam Type],
			recent_med.[Recent Medical Exam Date],
			CASE 
				WHEN [Exam Type Code] is NULL THEN 
					CASE 
						WHEN DATEDIFF(DD, [Custody Start], GETDATE()) >= 3 THEN 0
						ELSE 3 - DATEDIFF(DD, [Custody Start], GETDATE())
					END
				WHEN [Exam Type Code] = 700 THEN
					CASE
						WHEN DATEDIFF(DD, [Custody Start], GETDATE()) >= 30 THEN 0
						ELSE 30 - DATEDIFF(DD, [Custody Start], GETDATE())
					END
				WHEN [Exam Type Code] in (200, 300) THEN
					CASE
						WHEN DATEDIFF(DD, [Recent Medical Exam Date], GETDATE()) >= 365 THEN 0
						ELSE 365 - DATEDIFF(DD, [Recent Medical Exam Date], GETDATE())
					END
			END AS 'Days Until Next Medical Exam Due',		
			[Third Birthday],
			recent_dent.[Recent Dental Exam Date],
			CASE
				WHEN [Recent Dental Exam Date] is NULL THEN 
					CASE
						WHEN [Third Birthday] > [Custody Start] THEN
							CASE
								WHEN DATEDIFF(DD, [Third Birthday], GETDATE()) >= 90 THEN 0
								ELSE 90 - DATEDIFF(DD, [Third Birthday], GETDATE())
							END
						ELSE
							CASE
								WHEN DATEDIFF(DD, [Custody Start], GETDATE()) >= 90 THEN 0
								ELSE 90 - DATEDIFF(DD, [Custody Start], GETDATE())
							END
					END
				ELSE
					CASE
						WHEN DATEDIFF(DD, [Recent Dental Exam Date], GETDATE()) >= 180 THEN 0
						ELSE 180 - DATEDIFF(DD, [Recent Dental Exam Date], GETDATE())
					END
			END AS 'Days Until Next Dental Exam Due',
			[Fourth Birthday],
			recent_mh.[Recent MH Assessment Date],
			CASE
				WHEN [Recent MH Assessment Date] is NULL THEN 
					CASE
						WHEN [Fourth Birthday] > [Custody Start] THEN
							CASE
								WHEN DATEDIFF(DD, [Fourth Birthday], GETDATE()) >= 30 THEN 0
								ELSE 30 - DATEDIFF(DD, [Fourth Birthday], GETDATE())
							END
						ELSE
							CASE
								WHEN DATEDIFF(DD, [Custody Start], GETDATE()) >= 30 THEN 0
								ELSE 30 - DATEDIFF(DD, [Custody Start], GETDATE())
							END
					END
				ELSE NULL
			END AS 'Days Until MH Assessment Due'
		from (
			select distinct Region,
				County,
				[Case ID],
				[Child ID],
				[Child Name],
				[Custody Start],
				[Third Birthday],
				[Fourth Birthday]
			from @exams_in_custody_episodes
		) base

		-- Add in most recent Medical
		left join (
			select med_info.[Child ID],
				med_info.[Exam Date] as 'Recent Medical Exam Date',
				med_info.[Exam Type Code],
				med_info.[Exam Type]
			from (
				select [Child ID],
					max([Exam Sequence]) as 'Max Medical Sequence'
				from @exams_in_custody_episodes
				where [Exam Type Code] in (
					200,	-- EPSDT 
					300,	-- Medical Exam
					700		-- Initial Health Screening
				)
				group by [Child ID]
			) med_base
			left join (
				select [Child ID],
					[Exam Sequence],
					[Exam Date],
					[Exam Type Code],
					[Exam Type]
				from @exams_in_custody_episodes
			) med_info
			on med_base.[Child ID] = med_info.[Child ID]
				and med_base.[Max Medical Sequence] = med_info.[Exam Sequence]
		) recent_med
		on recent_med.[Child ID] = base.[Child ID]

		-- Recent Dental Exam
		left join (
			select distinct [Child ID],
				max([Exam Date]) as 'Recent Dental Exam Date'
			from @exams_in_custody_episodes
			where [Exam Type Code] = 100 -- Dental
			group by [Child ID]
		) recent_dent
		on recent_dent.[Child ID] = base.[Child ID]

		-- Recent Mental Health Assessment
		left join (
			select distinct [Child ID],
				max([Exam Date]) as 'Recent MH Assessment Date'
			from @exams_in_custody_episodes
			where [Exam Type Code] in (
			400,	-- Psychiatric
			500,	-- Psychological
			900		-- Mental Health Assessment
			)
			group by [Child ID]
		) recent_mh
		on recent_mh.[Child ID] = base.[Child ID]

		-- Add ASWS/Worker
		left join (
			select [Case ID],
				[Worker Name],
				[ASWS Name]
			from @workers_per_case
		) asws_wrkr
		on base.[Case ID] = asws_wrkr.[Case ID]
	) query
	return
end
*/

select [Data Date],
	Region,
	County,
	[ASWS Name],
	[Worker Name],
	[Child Name],
	[Custody Start],
	[Recent Medical Exam Date],
	[Days Until Next Medical Exam Due] as 'Days Until Medical Exam Due',
	[Recent Dental Exam Date],
	[Days Until Next Dental Exam Due] as 'Days Until Dental Exam Due',
	[Recent MH Assessment Date],
	[Days Until MH Assessment Due],
	[Indicator Column]
from EWB_Custody_Child_Medical_Dental_MH_Exams_Report()
