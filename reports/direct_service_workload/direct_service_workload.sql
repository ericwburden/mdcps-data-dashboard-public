select [Data Date],
	Region,
	County,
	[ASWS Name],
	[Worker Name],
	[Catalyst Description],
	[Case ID],
	[Case HH Name],
	[Child ID],
	[Child Name],
	[Case Weight]
from (
  select distinct
    GETDATE() as 'Data Date',
    cor_region as 'Region',
    service_county as  'County',
    rtrim(supervisor) as 'ASWS Name', 
    rtrim(worker) as 'Worker Name',
    catalyst_desc as 'Catalyst Description', 
    case_id as 'Case ID', 
    case_hh_name 'Case HH Name', 
    person_id 'Child ID', 
    person_name  'Child Name', 
    Custody_Start_date as 'Custody Start Date',
    Facility_Name as 'Facility Name',
    RESD_RESOURCE_NAME as 'Resource Name',
	  weights as 'Case Weight'
  from [dbo].[ZZZAr2K_Output_Unit_Title]
  left join MW_WORKER mw
  on [dbo].[ZZZAr2K_Output_Unit_Title].worker_id = mw.WORK_WORKER_ID
    and mw.WORK_ACTIVE_INDICATOR = 1 
    and mw.WORK_UNIT_TITLE='DIRECT SERVICE'
  where cor_region not in ('Special Investigation Unit','STATEWIDE')
) query
