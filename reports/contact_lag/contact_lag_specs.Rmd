---
title: "WORKER CONTACTS WITH CUSTODY CHILD REPORT"
output: word_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE)
output_type <- knitr::opts_knit$get('rmarkdown.pandoc.to')
```

### Row Colors:

  - Yellow: Warning, this item needs attention to ensure excellent practice. A record is given this designation if any of the following criteria are met:
    - The child has not received a qualifying contact within the past 15 days.
    - The child has not received any qualifying contacts this month and the current date is the 16th of the month or later.
    - The child has not received any qualifying contacts in their home this month and the current date is the 21st of the month or later.
    - The child has received only one qualifying contact this month, and the current date is the 21st of the month or later.
  - Red: This item is overdue or will be soon, needs urgent attention. A record is given this designation if any of the following criteria are met:
    - The child has not received a qualifying contact within the past 25 days.
    - The child has not received any qualifying contacts this month and the current date is the 21st of the month or later.
    - The child has not received any qualifying contacts in their home this month and the current date is the 26th of the month or later.
    - The child has received only one qualifying contact this month and the current date is the 26th of the month or later.
    
### Business Rules

- The report pulls all children currently in custody as the base population.
- Children with a current placement type of ICPC - Outgoing are excluded from the report.
- For each child, the first and most recent qualifying narratives in the current month are on the report. 
    - A qualifying narrative has a method of Face-to-Face and type is any other than Case Staffing, Attempted Contact, Closing Summary or Transfer Summary.
    - Any location is accepted when conducted by any MDCPS employee with the job title of ASWS, Family Protection Worker I, Family Protection Worker II, Family Protection Specialist, Family Protection Specialist Senior, or Family Protection Specialist Advanced
    - Only contacts that occurred after the child's most recent custody start date are included.
    - If the child has only received one qualifying contact in the month, the value for First Monthly Contact and Recent Monthly Contact will be the same, but the contact will only be counted once.
- The Region, County, ASWS and Worker are from the County of Responsibility on the Assign/Transfer screen.
- Days Since Last Visit are calculated from the most recent contact date in the current custody episode (or custody start if no recent contacts) and the data date.  This date is not shown in the report.

### Hidden Columns
- Highlight (Determines the color of the rows)