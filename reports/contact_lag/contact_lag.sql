with contacts as (
	-- Add in all Face to Face contacts
	select distinct napr.[Participant ID],
		--napr.[Narrative ID], -- Removing this eliminates same datetime duplicates
		narr.[Narrative DateTime],
		narr.[Narrative Location Code]
	from (
		select NAPR_PARTICIPANT as 'Participant ID',
			NAPR_NARRATIVE_ID as 'Narrative ID'
		from MW_NARR_PART_REL
		--Restrict to case narratives
		where NAPR_SYSTEM_TYPE = 'C'
	) napr

	-- Restrict to narratives with Custody Child ID's
	inner join (
		select CUST_CHILD_PERS_ID as 'Child ID',
			CUST_IN_CUSTODY_DT as 'Custody Start'
		from MW_CUSTODY_PER
		where CUST_CUSTODY_RELEASE_DT is NULL
	) cust_filter
	on cust_filter.[Child ID] = napr.[Participant ID]

	left join (
		select NARR_NARRATIVE_ID as 'Narrative ID',
			cast(NARR_DATE as datetime) + ISNULL(CAST(NARR_TIME as datetime), CAST(0 as datetime)) as 'Narrative DateTime',
			NARR_OWNER as 'Owner ID',
			NARR_LOCATION as 'Narrative Location Code'
		from MW_NARRATIVE
		where NARR_CONTACT_METHOD = 'FA2FA'
			and NARR_CONTACT_TYPE not in (
				'470',	-- Attempted Contact
				'480',	-- Closing Summary
				'600',	-- Transfer Summary
				'625'	-- Case Staffing
			)
	) narr
	on napr.[Narrative ID] = narr.[Narrative ID]
		and narr.[Narrative DateTime] >= cust_filter.[Custody Start]

	--Add worker title at time of the contact
	left join (
		select PIN_WORKER_ID as 'Worker ID',
			PIN_BEGIN_DT as 'PIN Begin Date',
			PIN_END_DT as 'PIN End Date',
			PIN_OCCUP_CODE as 'Worker Title Code'
		from MW_PIN_REL title_base
		where PIN_OCCUP_CODE in (
			'3745',		-- ASWS
			'4554',		-- FPWI
			'4555',		-- FPWII
			'4556',		-- FPS
			'4557',		-- FPSA
			'4558'		-- FPSS
		)
	) pin_title
	on narr.[Owner ID] = pin_title.[Worker ID]
		and pin_title.[PIN Begin Date] <= narr.[Narrative DateTime]
		and ISNULL(pin_title.[PIN End Date], GETDATE()) >= narr.[Narrative DateTime]
)

select GETDATE() as 'Data Date',
	Region,
	County,
	[ASWS Name],
	[Worker Name],
	[Child Name],
	[First Monthly Contact Date],
	[Recent Monthly Contact Date],
	[Days Since Last Visit],
	[Contacts This Month],
	[Contacts This Month (In Home)],
	CASE WHEN (
		[Days Since Last Visit] > 25
			or (DAY(GETDATE()) > 20 and [Contacts This Month] = 0)
			or (DAY(GETDATE()) > 25 and [Contacts This Month (In Home)] = 0)
			or (DAY(GETDATE()) > 25 and [Contacts This Month] = 1)
	) THEN 2
	ELSE CASE WHEN (
		[Days Since Last Visit] > 15
			or (DAY(GETDATE()) > 15 and [Contacts This Month] = 0)
			or (DAY(GETDATE()) > 20 and [Contacts This Month (In Home)] = 0)
			or (DAY(GETDATE()) > 20 and [Contacts This Month] = 1)
	) THEN 1
	ELSE 0
	END END AS 'Highlight'
from (
	select RTRIM(custody_info.Region) as 'Region',
		RTRIM(custody_info.County) as 'County',
		work_asws.[ASWS Name],
		work_asws.[Worker Name],
		child_info.[Child Name],
		--CONVERT(VARCHAR, work_asws.[Assignment Date], 0) as 'Assignment Date',
		CONVERT(VARCHAR, first_monthly_contact.[Narrative DateTime], 0) as 'First Monthly Contact Date',
		CONVERT(VARCHAR, recent_monthly_contact.[Narrative DateTime], 0) as 'Recent Monthly Contact Date',
		DATEDIFF(DD, ISNULL(recent_contact.[Narrative DateTime], custody_info.[Custody Start]), GETDATE()) as 'Days Since Last Visit',
		ISNULL(contacts_this_month.[Contacts This Month], 0) as 'Contacts This Month',
		ISNULL(inhome_contacts_this_month.[Contacts This Month], 0) as 'Contacts This Month (In Home)'

	-- Start with a base of all custody children and the case ID associated with
	-- that child's most recent open case during the custody episode.
	from (
		select MAX(casp.[Case ISN]) 'Case ISN',
			custody.[Child ID],
			custody.ISN as 'Custody ISN'
		from (
			select *
			from [Corrected_Custody_Episodes]
		) custody
		left join (
			select case_info.[Case ISN],
				CASP_CASE_ID as 'Case ID',
				CASP_PERS_ID as 'Case Person ID',
				case_info.[Case Start],
				case_info.[Case End]
			from MW_CASE_PERS casp
			left join (
				select ISN as 'Case ISN',
					CASE_ID as 'Case ID',
					CASE_DATE_OPENED as 'Case Start',
					CASE_DATE_CLOSED as 'Case End'
				from MW_CASE
			) case_info
			on casp.CASP_CASE_ID = case_info.[Case ID]
		) casp
		on casp.[Case Person ID] = custody.[Child ID]

		-- Restrict results to children in custody during the period
		-- and cases open during the period
		where custody.[Custody End] is NULL
			and casp.[Case End] is NULL

		group by custody.[Child ID],
			custody.ISN
	) base

	-- Add most recent placement type so we can later filter out ICPC - Outgoing
	left join (
		select PLAC_PERS_ID as 'Child ID',
			PLAC_FACILITY_TYPE as 'Placement Type Code'
		from MW_PLACEMENT plac_base
		inner join(
			select MAX(ISN) ISN,
				PLAC_PERS_ID as 'Child ID'
			from MW_PLACEMENT
			where PLAC_STATUS = 'A'
			group by PLAC_PERS_ID
		) recent_filter
		on plac_base.ISN = recent_filter.ISN
	) plac
	on base.[Child ID] = plac.[Child ID]

	-- Add in custody info
	left join (
		select *
		from Corrected_Custody_Episodes
	) custody_info
	on base.[Custody ISN] = custody_info.ISN

	-- Add in child info
	left join (
		select PERS_PERS_ID as 'Person ID',
			RTRIM(PERS_LAST_NAME) + ', ' + RTRIM(PERS_FIRST_NAME) as 'Child Name'
		from MW_PERS
	) child_info
	on base.[Child ID] = child_info.[Person ID]

	-- Add in case info
	left join (
		select ISN as 'Case ISN',
			CASE_ID as 'Case ID',
			casp.[Case Person ID]
		from MW_CASE case_info
		left join(
			select distinct CASP_CASE_ID as 'Case ID',
				CASP_PERS_ID as 'Case Person ID'
			from MW_CASE_PERS
		) casp
		on casp.[Case ID] = case_info.CASE_ID
	) case_info
	on base.[Case ISN] = case_info.[Case ISN]
		and base.[Child ID] = case_info.[Case Person ID]

	-- Add in worker/supervisor info
	left join(
		select assignment.[Worker Case ID],
			worker_info.[Worker Name],
			asws_info.[ASWS Name],
			assignment.[Assignment Date]
		from (
			select ISN as 'Assignment ISN',
				CASW_CASE_ID as 'Worker Case ID',
				CASW_WORKER_ID as 'Worker ID',
				CASW_SUPER_ID as 'ASWS ID',
				CASW_START_DATETIME as 'Assignment Date'
			from MW_CASE_WORKER
			where CASW_COUNTY_TYPE = 'COR'
		) assignment
		inner join (
			select MAX(ISN) as 'Max Assignment ISN',
				CASW_CASE_ID
			from MW_CASE_WORKER
			where CASW_COUNTY_TYPE = 'COR'
			group by CASW_CASE_ID
		) max_assignment_filter
		on assignment.[Assignment ISN] = max_assignment_filter.[Max Assignment ISN]
		left join (
			select WORK_WORKER_ID as 'Worker ID',
				RTRIM(RTRIM(WORK_LAST_NAME) + ', ' + RTRIM(WORK_FIRST_NAME) + ' ' + ISNULL(RTRIM(WORK_INIT), '')) as 'Worker Name'
			from MW_WORKER
		) worker_info
		on worker_info.[Worker ID] = assignment.[Worker ID]
		left join (
			select WORK_WORKER_ID as 'ASWS ID',
				RTRIM(RTRIM(WORK_LAST_NAME) + ', ' + RTRIM(WORK_FIRST_NAME) + ' ' + ISNULL(RTRIM(WORK_INIT), '')) as 'ASWS Name'
			from MW_WORKER
		) asws_info
		on asws_info.[ASWS ID] = assignment.[ASWS ID]
	) work_asws
	on work_asws.[Worker Case ID] = case_info.[Case ID]

	-- Join in first qualifying contact this month
	left join (
		select [Participant ID],
			MIN([Narrative DateTime]) as 'Narrative DateTime'
		from contacts
		where [Narrative DateTime] >= DATEADD(month, DATEDIFF(month, 0, GETDATE()), 0)
		group by [Participant ID]
	) first_monthly_contact
	on first_monthly_contact.[Participant ID] = base.[Child ID]
		and first_monthly_contact.[Narrative DateTime] >= [Custody Start]

	-- Join in most recent qualifying contact this month
	left join (
		select [Participant ID],
			MAX([Narrative DateTime]) as 'Narrative DateTime'
		from contacts
		where [Narrative DateTime] >= DATEADD(month, DATEDIFF(month, 0, GETDATE()), 0)
		group by [Participant ID]
	) recent_monthly_contact
	on recent_monthly_contact.[Participant ID] = base.[Child ID]
		and recent_monthly_contact.[Narrative DateTime] >= [Custody Start]


	-- Join in most recent contact for each child by Participant ID
	left join (
		select [Participant ID],
			MAX([Narrative DateTime]) as 'Narrative DateTime'
		from contacts
		group by [Participant ID]	
	) recent_contact
	on recent_contact.[Participant ID] = base.[Child ID]
		and recent_contact.[Narrative DateTime] >= [Custody Start]

	-- Join in count of contacts for the month
	left join (
		select [Participant ID],
			COUNT(*) as 'Contacts This Month'
		from contacts
		where [Narrative DateTime] >= DATEADD(month, DATEDIFF(month, 0, GETDATE()), 0)
		group by [Participant ID]
	) contacts_this_month
	on contacts_this_month.[Participant ID] = base.[Child ID]

	-- Join in count of contacts for the month that occurred in the home
	left join (
		select [Participant ID],
			COUNT(*) as 'Contacts This Month'
		from contacts
		where [Narrative DateTime] >= DATEADD(month, DATEDIFF(month, 0, GETDATE()), 0)
			and [Narrative Location Code] = 'HOME'
		group by [Participant ID]
	) inhome_contacts_this_month
	on inhome_contacts_this_month.[Participant ID] = base.[Child ID]

	-- Filter out ICPC - Outgoing placements
	where ISNULL(plac.[Placement Type Code], 1) != '9280'

) worker_child_visitation
