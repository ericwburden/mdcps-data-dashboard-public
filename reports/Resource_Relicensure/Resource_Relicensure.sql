select Distinct GETDATE() as 'Data Date',
                  rtrim(region) as 'Region',        
                  rtrim(county) as 'County',
					ltrim(rtrim(supr_last))+','+ltrim(rtrim(supr_first))+' ' +isnull(rtrim(supr_init),' ')'ASWS Name',
					ltrim(rtrim(work_last))+','+ltrim(rtrim(work_first))+' '+isnull(rtrim(work_init),' ') 'Worker Name',
					upper(res_name) as 'Resource Name', 
					upper(res_id) as 'Resource ID',
					upper(res_status) as 'Resource Status',
					case
					 when res_status = 'expir' then
					 case 
					 when res_id = 6980406 then 'Y'
					 when res_id =2495074 then 'Y' else 'N'
					 end
                     when res_status = ('Activ''NACPL') then 'null'
					
					end 'Children Currently placed in home',
					upper(lic_st_dt) as 'License Start Date',
					upper(lic_end_dt) as 'License End Date',
					case
					  when DATEDIFF(DD, GETDATE(), lic_end_dt) < 0 then 0
					  else DATEDIFF(DD, GETDATE(), lic_end_dt) 
					end as 'Days Until License Expires',
					case
						when res_status = 'EXPIR' then 2
						when DATEDIFF(DD, GETDATE(), lic_end_dt) <= 90 then 1
						else 0
					end as 'Indicator Column'
					
					
					from resource_relicense_table