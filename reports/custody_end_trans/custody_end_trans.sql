select GETDATE() as 'Data Date',
	Region,
	County,
	[ASWS Name],
	[Worker Name],
	[Case ID],
	[Child ID],
	[Child Name],
	[Case Start],
	cust.CUST_CUSTODY_RELEASE_DT as 'Custody End',
	cust.CUST_END_TRANS_DT as 'Custody End Transaction Date',
	DATEDIFF(DD, cust.CUST_CUSTODY_RELEASE_DT, cust.CUST_END_TRANS_DT) as 'Days to Enter Custody End'
from fod_child_case_crosswalk_yearly fcccy,
	MW_CUSTODY_PER cust 
where CUST_MOST_RECENT = 'Y'
	and fcccy.[Child ID] = cust.CUST_CHILD_PERS_ID
	and cust.CUST_END_TRANS_DT >= DATEADD(DD, -365, GETDATE())
	and cust.CUST_CUSTODY_RELEASE_DT > fcccy.[Case Start]
	and cust.CUST_CUSTODY_RELEASE_DT <= ISNULL(fcccy.[Case End], GETDATE())
	and fcccy.[Child Type] = 'Custody'
	and fcccy.[Custody Record ISN] = cust.ISN
order by 2, 3, 4, 5, 8