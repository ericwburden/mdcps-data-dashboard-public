 select [Data Date],
	Region,
	County,
	[ASWS Name],
	[Worker Name],
	[Unit Title],
	[Worker County],
	[Total Assigned],
	[Exceed Limit],
	CASE 
		WHEN [Total Assigned] > 0 THEN 1
		ELSE 0
	END AS 'Count Column'
 from (
	 SELECT distinct
		GETDATE() as 'Data Date',
		rtrim(region) as 'Region',
		rtrim(county) as 'County',
		ltrim(rtrim(super_last)) +', '+ltrim(rtrim(super_first)) as 'ASWS Name',
		ltrim(rtrim(worker_last)) + ', '+ltrim(rtrim(worker_first)) as 'Worker Name',
		Upper(super_pin) as 'Super Pin',
		Upper(unit_title) as 'Unit Title',
		Upper(worker_county) as 'Worker County',
		Upper(total_assigned) as 'Total Assigned',
		Upper(exceed_limit) as 'Exceed Limit',
		Upper(cty_super_cnt) as 'CTY Super Cnt',
		Upper(cty_worker_cnt) as 'CTY Worker Cnt',
		Upper(cty_exceed_cnt) as 'CTY Exceed Cnt',
		Upper(cty_exceed_pct) as 'CTY Exceed Pct',
		Upper(reg_super_cnt) as 'Reg Super Cnt',
		Upper(reg_worker_cnt) as 'Reg Worker Cnt',
		Upper(reg_exceed_cnt) as 'Reg Exceed Cnt',
		Upper(reg_exceed_pct) as 'Reg Exceed Pct',
		Upper(st_super_cnt) as 'St Super Cnt',
		Upper(st_worker_cnt) as 'St worker Cnt',
		Upper(st_exceed_cnt) as 'St Exceed Cnt',
		Upper(st_exceed_pct) as 'St Exceed Pct'
	FROM output_tb1
) ar2_query
