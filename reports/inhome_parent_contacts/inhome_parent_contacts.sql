SELECT [Data Date]
	,[Region]
	,[County]
	,[ASWS Name]
	,[Worker Name]
	,[Parent Name]
	,[Child Name]
	,[Recent Monthly Contact Date]
	,[Days Since Last Contact]
	,[Contacts This Month]
	,[Contacts This Month (In Home, F2F)]
	,CASE WHEN (
		[Days Since Last Contact] > 25
			or (DAY(GETDATE()) > 20 and [Contacts This Month] = 0)
			or (DAY(GETDATE()) > 25 and [Contacts This Month (In Home, F2F)] = 0)
			or (DAY(GETDATE()) > 25 and [Contacts This Month] = 1)
	) THEN 2
	ELSE CASE WHEN (
		[Days Since Last Contact] > 15
			or (DAY(GETDATE()) > 15 and [Contacts This Month] = 0)
			or (DAY(GETDATE()) > 20 and [Contacts This Month (In Home, F2F)] = 0)
			or (DAY(GETDATE()) > 20 and [Contacts This Month] = 1)
	) THEN 1
	ELSE 0
	END END AS 'Indicator Column'
FROM [fod_worker_inhome_par_contacts] 