select GETDATE() as 'Data Date',
  open_inquiries_report.[Intake Region] as 'Region',
	open_inquiries_report. [Intake County] as 'County',
	open_inquiries_report.[Inquiry Supervisor] as 'ASWS Name',
	open_inquiries_report.[Inquiry Worker] as 'Worker Name',
	open_inquiries_report.[HH Name],
	open_inquiries_report.[Intake Date],
	open_inquiries_report.[Days to Decision] as 'Days Open'
from (
	-- Start with a base of all resource inquiries
	select rtrim(coreg.region_name) as 'Intake Region',
		rtrim(coreg.county_name) as 'Intake County',
		base.[Resource ID],
		inrp.[HH ID],
		inrp.[HH Name],
		ISNULL(restat_typ.[Resource Status], 'Not Approved') as 'Resource Status',
		intr.[Intake Date],
		intr.[Intake Status],
		int_wrkr.[Intake Worker],
		intr.[Expedited Intake],
		inq_sup.[Inquiry Supervisor],
		inq_wrkr.[Inquiry Worker],
		base.[Resource Worker ID],
		assh.[Assessment Worker ID],
		assh.[Assessment Supervisor ID],
		base.[Inquiry Status],
		base.[Inquiry Start],
		base.[Inquiry End],
		base.[Packet Sent],
		base.[Date Training Completed],
		base.[Date Information Provided],
		base.[Home Study Requirement],
		assh.[Home Study Decision],
		assh.[Home Study Decision Date],
		resp_coreg.region_name as 'Responsible Region',
		resp_coreg.county_name as 'Responsible County',
		resd.[Resource Name],
		wrkr.[Resource Worker],
		resd_typ.[Resource Type],
		resd.[Start Date],
		resd.[End Date],
		resl.[License Status],
		ISNULL(exp_pend.[Expedited Pending], 'False') as 'Expedited Pending',
		CASE WHEN base.[Home Study Requirement] = 'N'
			THEN base.[Inquiry End]
			ELSE 
				CASE WHEN assh.[Home Study Decision] = 'D'
					THEN assh.[Home Study Decision Date]
					END
			END as 'Denied Date',
		license.[Initial License Start Date] as 'Approved Date',
		DATEDIFF(DD,
			intr.[Intake Date],
			ISNULL(
				CASE WHEN base.[Home Study Requirement] = 'N'
					THEN base.[Inquiry End]
					ELSE 
					CASE WHEN assh.[Home Study Decision] = 'D'
						THEN assh.[Home Study Decision Date]
						END
				END,
				ISNULL(
					license.[Initial License Start Date],
					GETDATE()
				)
			)
		) as 'Days to Decision',
		CASE WHEN (base.[Home Study Requirement] = 'N' OR assh.[Home Study Decision] = 'D')
			THEN 'Denied'
			ELSE CASE WHEN license.[Initial License Start Date] is not NULL
				THEN 'Approved'
				ELSE 'Pending'
				END
			END as 'Decision Status'


	from (
		select REIN_RESOURCE_ID as 'Resource ID',
			REIN_WORKER_ID as 'Resource Worker ID',
			REIN_STATUS as 'Inquiry Status',
			REIN_START_DT as 'Inquiry Start',
			REIN_END_DT as 'Inquiry End',
			REIN_INT_PACKET_SENT_DT as 'Packet Sent',
			REIN_TRAIN_COMPLETE_DT as 'Date Training Completed',
			REIN_INFO_PROVIDED_DT as 'Date Information Provided',
			REIN_HOME_STUDY_REQ as 'Home Study Requirement'
		from MW_RES_INQUIRY
	) base 

	--Add in supervisor assignments
	left join (
		select ISN, 
			CASW_WORKER_ID as 'Assignment Worker ID',
			CASW_SUPER_ID as 'Assignment Super ID'
		from MW_CASE_WORKER
		inner join (
			select MAX(ISN) max_isn,
				CASW_WORKER_ID as 'Recent Assignment Worker ID'
			from MW_CASE_WORKER
			group by CASW_WORKER_ID
		) recent_filter
		on ISN = recent_filter.max_isn
	) assignment
	on base.[Resource Worker ID] = assignment.[Assignment Worker ID]

	-- Add intake information
	left join (
		select INTR_INTAKE_ID as 'Intake ID',
			INTR_INTAKE_CO as 'Intake County',
			INTR_INTAKE_DT as 'Intake Date',
			INTR_INTAKE_STATUS as 'Intake Status',
			INTR_CURR_WORKER_ID as 'Intake Worker ID',
			CASE WHEN INTR_EXPEDITED_RESRC = 1
				THEN 'TRUE'
				ELSE 'FALSE'
				END as 'Expedited Intake'
		from MW_INT_REPORT
		where INTR_INTAKE_TYPE = 'RESIN'
	) intr
	on intr.[Intake ID] = base.[Resource ID]

	left join (
		select INRP_INTAKE_ID as 'Intake ID',
			INRP_C_PERS_ID as 'HH ID',
			hh_name.[HH Name]
		from MW_INT_RPT_PERS hh_base
		left join (
			select RTRIM(PERS_LAST_NAME) + ',' + RTRIM(PERS_FIRST_NAME) as 'HH Name',
				PERS_PERS_ID as 'HH ID'
			from MW_PERS
		) hh_name
		on hh_name.[HH ID] = hh_base.INRP_C_PERS_ID
		where INRP_PERS_HH_STAT_IND = 'X'
	) inrp
	on inrp.[Intake ID] = intr.[Intake ID]

	left join (
		select *
		from COUNTY_REGION
	) coreg
	on coreg.county_code = intr.[Intake County]

	-- Add home studies
	left join (
		select ASSH_INTAKE_ID as 'Intake ID',
			ASSH_SUPER_ID as 'Assessment Supervisor ID',
			ASSH_OWNER_ID as 'Assessment Worker ID',
			ISNULL(ASSH_SO_REC, ASSH_SUPER_REC) as 'Home Study Decision',
			ISNULL(ASSH_SO_REC_DATE, ASSH_SUPER_REC_DATE) as 'Home Study Decision Date'
		from MW_ASSESS_HOME
	) assh
	on assh.[Intake ID] = base.[Resource ID]

	-- Add license status
	left join (
		select RESL_RESOURCE_ID as 'Resource ID',
			RESL_LICENSE_STATUS as 'License Status'
		from MW_RES_LICENSE resl_base

		--Filter by most recent license status
		inner join (
			select MAX(ISN) ISN,
				RESL_RESOURCE_ID as 'Resource ID'
			from MW_RES_LICENSE
			group by RESL_RESOURCE_ID
		) resl_filter
		on resl_filter.ISN = resl_base.ISN
	) resl
	on resl.[Resource ID] = base.[Resource ID]

	-- Add in initial license approval
	left join (
		select RESL_RESOURCE_ID as 'Resource ID',
			RESL_START_DT as 'Initial License Start Date'
		from MW_RES_LICENSE
		where RESL_INIT_REN_IND = 'I'
	) license
	on base.[Resource ID] = license.[Resource ID]

	-- Add resource information
	left join (
		select RESD_RESOURCE_ID as 'Resource ID',
			RESD_RESOURCE_NAME as 'Resource Name',
			RESD_TYPE as 'Resource Type Code',
			RESD_RESP_CO as 'Responsible County',
			RESD_WORKER_ID as 'Resource Worker ID',
			RESD_STATUS as 'Resource Status Code',
			RESD_START_DT as 'Start Date',
			RESD_END_DT as 'End Date'
		from MW_RESRC_DIR
	) resd
	on resd.[Resource ID] = base.[Resource ID]

	-- Add county and region names
	left join (
		select *
		from COUNTY_REGION
	) resp_coreg
	on resp_coreg.county_code = resd.[Responsible County]

	-- Add in current expedited service indicator
	left join (
		select MAX(RESS_START_DT) as 'Max Date', 
			RESS_RESOURCE_ID as 'Resource ID',
			'True ' as 'Expedited Pending'
		from MW_RES_SERVICES
		where RESS_SERVICE_TYPE_CD = '9410'
			and RESS_END_DT is NULL
		group by RESS_RESOURCE_ID
	) exp_pend
	on exp_pend.[Resource ID] = base.[Resource ID]

	-- Replace codes with descriptions
	left join (
		select CODE_TABLE_VALUE as 'Resource Type Code',
			CODE_SHORT_DESC as 'Resource Type'
		from MW_CODE_TABLE
		where CODE_TABLE_NAME = 'RESTYPE'
	) resd_typ
	on resd_typ.[Resource Type Code] = resd.[Resource Type Code]

	left join (
		select CODE_TABLE_VALUE as 'Resource Status Code',
			CODE_SHORT_DESC as 'Resource Status'
		from MW_CODE_TABLE
		where CODE_TABLE_NAME = 'LICSTATS'
	) restat_typ
	on restat_typ.[Resource Status Code] = resd.[Resource Status Code]

	-- Add in various worker names
	left join (
		select WORK_WORKER_ID as 'Worker ID',
			RTRIM(RTRIM(WORK_LAST_NAME) + ', ' + RTRIM(WORK_FIRST_NAME) + ' ' + ISNULL(RTRIM(WORK_INIT), '')) as 'Resource Worker'
		from MW_WORKER
	) wrkr
	on wrkr.[Worker ID] = resd.[Resource Worker ID]

	left join (
		select WORK_WORKER_ID as 'Worker ID',
			RTRIM(RTRIM(WORK_LAST_NAME) + ', ' + RTRIM(WORK_FIRST_NAME) + ' ' + ISNULL(RTRIM(WORK_INIT), '')) as 'Intake Worker'
		from MW_WORKER
	) int_wrkr
	on int_wrkr.[Worker ID] = intr.[Intake Worker ID]

	left join (
		select WORK_WORKER_ID as 'Worker ID',
			RTRIM(RTRIM(WORK_LAST_NAME) + ', ' + RTRIM(WORK_FIRST_NAME) + ' ' + ISNULL(RTRIM(WORK_INIT), '')) as 'Inquiry Worker'
		from MW_WORKER
	) inq_wrkr
	on inq_wrkr.[Worker ID] = base.[Resource Worker ID]

	left join (
		select WORK_WORKER_ID as 'Supervisor ID',
			RTRIM(RTRIM(WORK_LAST_NAME) + ', ' + RTRIM(WORK_FIRST_NAME) + ' ' + ISNULL(RTRIM(WORK_INIT), '')) as 'Inquiry Supervisor'
		from MW_WORKER
	) inq_sup
	on inq_sup.[Supervisor ID] = assignment.[Assignment Super ID]
) open_inquiries_report

where open_inquiries_report.[Decision Status] = 'Pending'
