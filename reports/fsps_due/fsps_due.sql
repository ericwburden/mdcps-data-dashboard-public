with query as (
	select GETDATE() as 'Data Date',
		Region,
		County,
		[ASWS Name],
		[Worker Name],
		[HH Name],
		[Last Approved FSP],
		[Last FSP Approved Date],
		[Next FSP Due],
		case when [Days Overdue] <= 0
			then ABS([Days Overdue])
			else 0
			end as 'Days Remaining',
		case when [Days Overdue] > 0
			then [Days Overdue]
			else 0
			end as 'Days Overdue'
	from (
		select base.[Case ID],
			coreg.Region,
			coreg.County,
			wrkr.[Worker Name], 
			asws.[ASWS Name],
			hh.[HH ID],
			hh.[HH Name],
			case_start.[Case Start],
			case when fsp.[FSP Approval Date] is NULL
			  then 'Initial FSP (Pending)'
			  else fsp.[Plan Type]
			  end as 'Last Approved FSP',
			fsp.[Plan Status],
			fsp.[FSP Approval Date] as 'Last FSP Approved Date',
			case when fsp.[FSP Approval Date] is NULL or case_start.[Case Start] > fsp.[FSP Approval Date]
			  then DATEADD(DD, 30, case_start.[Case Start])
			  else DATEADD(DD, 90, ir_date.[Latest I/R Approval]) 
			  end as 'Next FSP Due',

			-- Calculate as 90 days from last approved FSP, unless the case opened after the last FSP or there is no FSP, 
			-- then calculate as 30 days after case start
			case when fsp.[FSP Approval Date] is NULL or case_start.[Case Start] > fsp.[FSP Approval Date]
			  then DATEDIFF(DD, DATEADD(DD, 30, case_start.[Case Start]), GETDATE())
			  else DATEDIFF(DD, DATEADD(DD, 90, ir_date.[Latest I/R Approval]), GETDATE()) 
			  end as 'Days Overdue'

		-- start with a base of all actively assigned caseworkers and case id's
		from (
			select distinct CASW_CASE_ID as 'Case ID',
				CASW_COUNTY as 'County Code',
				CASW_WORKER_ID as 'Worker ID',
				CASW_SUPER_ID as 'Supervisor ID'
			from MW_CASE_WORKER
			where CASW_COUNTY_TYPE = 'COR'
				and CASW_STATUS = 'A'
		) base

		-- Add HH ID
		left join (
			-- Get HH Name for most recent HH assigned per case
			select distinct casp_base.[Case ID],
				casp_base.[HH ID],
				hh_name.[HH Name]
			from (
				select ISN as 'CASP ISN',
					CASP_CASE_ID as 'Case ID',
					CASP_PERS_ID as 'HH ID'
				from MW_CASE_PERS
				where CASP_TYPE = 'HH'
					and CASP_STATUS = 'A'
					and ISNULL(CASP_RSN_CHANGE, 1) != 150
			) casp_base
			inner join (
				select MAX(ISN) 'Max CASP ISN',
					CASP_PERS_ID as 'HH ID'
				from MW_CASE_PERS
				where CASP_TYPE = 'HH'
					and CASP_STATUS = 'A'
					and ISNULL(CASP_RSN_CHANGE, 1) != 150
				group by CASP_PERS_ID
			) rec_filter
			on casp_base.[CASP ISN] = rec_filter.[Max CASP ISN]
			left join (
				select RTRIM(PERS_LAST_NAME) + ', ' + RTRIM(PERS_FIRST_NAME) as 'HH Name',
					PERS_PERS_ID as 'Person ID'
				from MW_PERS
			) hh_name
			on hh_name.[Person ID] = casp_base.[HH ID]
		) hh
		on hh.[Case ID] = base.[Case ID]

		-- Add county and region names
		left join (
			select county_code,
				RTRIM(county_name) as 'County',
				RTRIM(region_name) as 'Region'
			from COUNTY_REGION
		) coreg
		on base.[County Code] = coreg.county_code

		-- Add worker/supervisor names, filter out Aging cases
		inner join (
			select RTRIM(RTRIM(WORK_LAST_NAME) + ', ' + RTRIM(WORK_FIRST_NAME) + ' ' + ISNULL(RTRIM(WORK_INIT), '')) as 'Worker Name',
				WORK_WORKER_ID as 'Worker ID'
			from MW_WORKER
			where WORK_UNIT_TITLE != 'Aging'
		) wrkr
		on wrkr.[Worker ID] = base.[Worker ID]

		left join (
			select RTRIM(RTRIM(WORK_LAST_NAME) + ', ' + RTRIM(WORK_FIRST_NAME) + ' ' + ISNULL(RTRIM(WORK_INIT), '')) as 'ASWS Name',
				WORK_WORKER_ID as 'ASWS ID'
			from MW_WORKER
		) asws
		on asws.[ASWS ID] = base.[Supervisor ID]

		-- Add latest Initial/Review FSP Approval Dates
		left join (
			select fsp_base.[Case ID],
				fsp_base.[Latest I/R Approval]
			from (
				select ISN as 'FSP ISN',
					FSPL_CASE_ID as 'Case ID',
					FSPL_PLAN_TYPE as 'Plan Type Code',
					FSPL_APPROVE_DT as 'Latest I/R Approval',
					FSPL_TIMESTAMP as 'Timestamp'
				from MW_FSP
				where (FSPL_STATUS = 'A' or FSPL_STATUS = 'C')
					and FSPL_PLAN_TYPE in ('010', '030')
			) fsp_base
			--Filter by most recent approval date
			inner join (
				select MAX(FSPL_APPROVE_DT) as 'Max Approval Date',
					FSPL_CASE_ID as 'Case ID'
				from MW_FSP
				where (FSPL_STATUS = 'A' or FSPL_STATUS = 'C')
					and FSPL_PLAN_TYPE in ('010', '030')
				group by FSPL_CASE_ID
			) rec_filter
			on [Latest I/R Approval] = rec_filter.[Max Approval Date]
				and fsp_base.[Case ID] = rec_filter.[Case ID]
			-- In cases with multilple records with the same approval date, filter also by timestamp
			inner join (
				select MAX(FSPL_TIMESTAMP) as 'Max Timestamp',
					FSPL_APPROVE_DT as 'Approval Date',
					FSPL_CASE_ID as 'Case ID'
				from MW_FSP
				where (FSPL_STATUS = 'A' or FSPL_STATUS = 'C')
					and FSPL_PLAN_TYPE in ('010', '030')
				group by FSPL_APPROVE_DT,
					FSPL_CASE_ID
			) mult_filter
			on fsp_base.[Latest I/R Approval] = mult_filter.[Approval Date]
				and fsp_base.[Case ID] = mult_filter.[Case ID]
				and fsp_base.Timestamp = mult_filter.[Max Timestamp]
		) ir_date
		on ir_date.[Case ID] = base.[Case ID]

		--Add Case Start Date
		left join (
			select CASE_ID as 'Case ID',
				CASE_DATE_OPENED as 'Case Start'
			from MW_CASE

			--Filter for most recent case open period
			inner join (
				select MAX(ISN) as 'Max ISN',
					CASE_ID as 'Case ID'
				from MW_CASE
				group by CASE_ID
			) max_isn
			on max_isn.[Max ISN] = ISN
		) case_start
		on case_start.[Case ID] = base.[Case ID]

		-- Add latest FSP (Any Type) and Approval Dates
		-- Filter out records where latest plan type is Final FSP
		left join (
			select fsp_base.[Case ID],
				fsp_base.[FSP Approval Date],
				plan_type.[Plan Type],
				fsp_base.[Plan Status]
			from (
				select FSPL_CASE_ID as 'Case ID',
					FSPL_PLAN_TYPE as 'Plan Type Code',
					FSPL_APPROVE_DT as 'FSP Approval Date',
					FSPL_STATUS as 'Plan Status',
					FSPL_TIMESTAMP as 'Timestamp'
				from MW_FSP
				--Filter by most recent approval date
				inner join (
					select MAX(FSPL_APPROVE_DT) as 'Max Approval Date',
						FSPL_CASE_ID as 'Case ID'
					from MW_FSP
					group by FSPL_CASE_ID
				) rec_filter
				on FSPL_APPROVE_DT = rec_filter.[Max Approval Date]
					and FSPL_CASE_ID = rec_filter.[Case ID]
				-- In cases with multilple records with the same approval date, filter also by timestamp
				inner join (
					select MAX(FSPL_TIMESTAMP) as 'Max Timestamp',
						FSPL_APPROVE_DT as 'Approval Date',
						FSPL_CASE_ID as 'Case ID'
					from MW_FSP
					group by FSPL_APPROVE_DT,
						FSPL_CASE_ID
				) mult_filter
				on FSPL_APPROVE_DT = mult_filter.[Approval Date]
					and FSPL_CASE_ID = mult_filter.[Case ID]
					and FSPL_TIMESTAMP = mult_filter.[Max Timestamp]
			) fsp_base
			left join (
				select CODE_TABLE_VALUE as 'Plan Type Code',
					RTRIM(CODE_SHORT_DESC) as 'Plan Type'
				from MW_CODE_TABLE
				where CODE_TABLE_NAME = 'FSP_APPR'
			) plan_type
			on fsp_base.[Plan Type Code] = plan_type.[Plan Type Code]
		) fsp
		on fsp.[Case ID] = base.[Case ID]
	) fsp_report
	where ([Last Approved FSP] != 'Final FSP' or [Case Start] >= [Last FSP Approved Date])
)
select * from query
