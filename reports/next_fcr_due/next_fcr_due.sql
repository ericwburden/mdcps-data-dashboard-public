with hh_names as (
	-- Get HH Name for most recent HH assigned per case
	select distinct cast(casp_base.[Case ID] as int) as 'Case ID',
		cast(casp_base.[HH ID] as int) as 'HH ID',
		cast(hh_name.[HH Name] as varchar(45)) as 'HH Name'
	from (
		select ISN as 'CASP ISN',
			CASP_CASE_ID as 'Case ID',
			CASP_PERS_ID as 'HH ID'
		from MW_CASE_PERS
		where CASP_TYPE = 'HH'
			and CASP_STATUS = 'A'
			and ISNULL(CASP_RSN_CHANGE, 1) != 150
	) casp_base
	inner join (
		select MAX(ISN) 'Max CASP ISN',
			CASP_PERS_ID as 'HH ID'
		from MW_CASE_PERS
		where CASP_TYPE = 'HH'
			and CASP_STATUS = 'A'
			and ISNULL(CASP_RSN_CHANGE, 1) != 150
		group by CASP_PERS_ID
	) rec_filter
	on casp_base.[CASP ISN] = rec_filter.[Max CASP ISN]
	left join (
		select RTRIM(PERS_LAST_NAME) + ', ' + RTRIM(PERS_FIRST_NAME) as 'HH Name',
			PERS_PERS_ID as 'Person ID'
		from MW_PERS
	) hh_name
	on hh_name.[Person ID] = casp_base.[HH ID]
),
query as (
	select GETDATE() as 'Data Date',
		Region,
		County,
		[ASWS Name],
		[Worker Name],
		--[HH ID],
		[HH Name],
		[Child IDs],
		[Child Names],
		[Case Start],
		[Last Review Date],
		[Next Due Date],
		case when (GETDATE() <= [Next Due Date])
			then DATEDIFF(DD, GETDATE(), [Next Due Date]) 
			else 0
			end as 'Days Remaining',
		case when (GETDATE() > [Next Due Date])
			then DATEDIFF(DD, [Next Due Date], GETDATE()) 
			else 0
			end as 'Days Overdue'
	from (
		select base.[Case ID],
			base.Region,
			base.County,
			wrkr.[Worker Name], 
			asws.[ASWS Name],
			hh.[HH ID],
			hh.[HH Name],
			id_col.[Child IDs],
			name_col.[Child Names],
			base.[Case Start],
			fcr.[Review Date] as 'Last Review Date',
			DATEADD(MM, 6, isnull([Review Date], [Case Start])) as 'Next Due Date'



		-- start with a base of all actively assigned caseworkers and case id's
		from (
			select crosswalk.[Case ID],
				Region,
				County,
				[Worker ID],
				[Supervisor ID],
				[Case Start]
			from (
				select distinct [Case ID],
					Region,
					County,
					[Case Start]
				from dbo.Custody_Case_Crosswalk_Fun(GETDATE(), GETDATE())
			) crosswalk
			left join (
				select distinct CASW_CASE_ID as 'Case ID',
					CASW_WORKER_ID as 'Worker ID',
					CASW_SUPER_ID as 'Supervisor ID'
				from MW_CASE_WORKER
				where CASW_COUNTY_TYPE = 'COR'
					and CASW_STATUS = 'A'
			) casw
			on crosswalk.[Case ID] = casw.[Case ID]
		) base

		-- Add HH ID
		left join (
			-- Get HH Name for most recent HH assigned per case
			select *
			from hh_names
		) hh
		on hh.[Case ID] = base.[Case ID]

		-- Add worker/supervisor names, filter out Aging cases
		left join (
			select RTRIM(RTRIM(WORK_LAST_NAME) + ', ' + RTRIM(WORK_FIRST_NAME) + ' ' + ISNULL(RTRIM(WORK_INIT), '')) as 'Worker Name',
				WORK_WORKER_ID as 'Worker ID'
			from MW_WORKER
			where WORK_UNIT_TITLE != 'Aging'
		) wrkr
		on wrkr.[Worker ID] = base.[Worker ID]

		left join (
			select RTRIM(RTRIM(WORK_LAST_NAME) + ', ' + RTRIM(WORK_FIRST_NAME) + ' ' + ISNULL(RTRIM(WORK_INIT), '')) as 'ASWS Name',
				WORK_WORKER_ID as 'ASWS ID'
			from MW_WORKER
		) asws
		on asws.[ASWS ID] = base.[Supervisor ID]

		-- Add in last FCR
		left join (
			select BOAR_CASE_ID as 'Case ID',
				MAX(BOAR_ACTUAL_REV_DT) as 'Review Date'
			from MW_BOARD_CONF_REV
			where BOAR_ACTUAL_REV_DT <= GETDATE()
				and BOAR_REVIEW_NOT_HELD is NULL
			group by BOAR_CASE_ID
		) fcr
		on fcr.[Case ID] = base.[Case ID]
			and fcr.[Review Date] >= base.[Case Start]

		-- Add in case child IDs
		left join (
			select [Case ID],
				STUFF(
						COALESCE('<br/>' + cast([Child 1] as varchar), '') +
						COALESCE('<br/>' + cast([Child 2] as varchar), '') +
						COALESCE('<br/>' + cast([Child 3] as varchar), '') +
						COALESCE('<br/>' + cast([Child 4] as varchar), '') +
						COALESCE('<br/>' + cast([Child 5] as varchar), '') +
						COALESCE('<br/>' + cast([Child 6] as varchar), '') +
						COALESCE('<br/>' + cast([Child 7] as varchar), '') +
						COALESCE('<br/>' + cast([Child 8] as varchar), '') +
						COALESCE('<br/>' + cast([Child 9] as varchar), '') +
						COALESCE('<br/>' + cast([Child 10] as varchar), ''),
						1, 5, ''
					) 'Child IDs'
			from (
				select [Case ID],
					[Child ID],
					'Child ' + CAST(
						ROW_NUMBER() over (
							PARTITION BY [Case ID]
							ORDER BY [Child ID]
						) as varchar
					) as 'Child Number'
				from dbo.Custody_Case_Crosswalk_Fun(GETDATE(), GETDATE())
			) base
			pivot (
				max([Child ID])
				for [Child Number] in (
					[Child 1], 
					[Child 2], 
					[Child 3], 
					[Child 4], 
					[Child 5], 
					[Child 6], 
					[Child 7], 
					[Child 8], 
					[Child 9], 
					[Child 10]
				)
			) piv
		) id_col
		on id_col.[Case ID] = base.[Case ID]

		-- Add in case child IDs
		left join (
			select [Case ID],
				STUFF(
						COALESCE('<br/>' + cast([Child 1] as varchar), '') +
						COALESCE('<br/>' + cast([Child 2] as varchar), '') +
						COALESCE('<br/>' + cast([Child 3] as varchar), '') +
						COALESCE('<br/>' + cast([Child 4] as varchar), '') +
						COALESCE('<br/>' + cast([Child 5] as varchar), '') +
						COALESCE('<br/>' + cast([Child 6] as varchar), '') +
						COALESCE('<br/>' + cast([Child 7] as varchar), '') +
						COALESCE('<br/>' + cast([Child 8] as varchar), '') +
						COALESCE('<br/>' + cast([Child 9] as varchar), '') +
						COALESCE('<br/>' + cast([Child 10] as varchar), ''),
						1, 5, ''
					) 'Child Names'
			from (
				select [Case ID],
					[Child Name],
					'Child ' + CAST(
						ROW_NUMBER() over (
							PARTITION BY [Case ID]
							ORDER BY [Child ID]
						) as varchar
					) as 'Child Number'
				from dbo.Custody_Case_Crosswalk_Fun(GETDATE(), GETDATE())
			) base
			pivot (
				max([Child Name])
				for [Child Number] in (
					[Child 1], 
					[Child 2], 
					[Child 3], 
					[Child 4], 
					[Child 5], 
					[Child 6], 
					[Child 7], 
					[Child 8], 
					[Child 9], 
					[Child 10]
				)
			) piv
		) name_col
		on name_col.[Case ID] = base.[Case ID]
	) fsp_report
)

select * 
from query
