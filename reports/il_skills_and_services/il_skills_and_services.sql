select *,
	case
		when [Days Since IL Skill/Service] >= 30 then 2
		when [Days Since IL Skill/Service] > 25 and [IL Skill/Service in Current Month] is NULL then 2
		when [Days Since IL Skill/Service] >= 25 then 1
		when [Days Since IL Skill/Service] > 20 and [IL Skill/Service in Current Month] is NULL then 1
		else 0
	end as 'Indicator Column'
from fod_il_services
