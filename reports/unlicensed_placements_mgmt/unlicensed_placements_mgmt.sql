SELECT  distinct  GETDATE() as 'Data Date',
  rtrim(region) as 'Region',
  rtrim(county) as 'County' ,              
  RTRIM(RTRIM(sup_last) + ', ' + RTRIM(sup_first) + ' ' + ISNULL(RTRIM(sup_mi), '')) as 'ASWS Name',
  RTRIM(RTRIM(work_last) + ', ' + RTRIM(work_first) + ' ' + ISNULL(RTRIM(work_mi), '')) as 'Worker Name',
  child_id as 'Child ID',
  (ltrim(rtrim(child_last)) + ', ' + ltrim(rtrim(child_first))) as ' Child Name',
  cust_st_dt as 'Custody start Date',
  plac_status as 'Placement status',
  plac_st_dt as 'Placement Start Date',
  plac_days as 'Days in Placement',
  upper(plac_res_name) as 'Resource Name',
  rtrim(resource_region) as 'Resource Region',
  Case
    when sibling_group IS null then 'N'
    else sibling_group
  end as 'Sibling Group',
  plac_type as 'Placement Type',
  court_ordered as 'Placement Court Ordered',
  plac_over_90 as 'Placement over 90'
FROM SLS319_BLENDER
