select GETDATE() as 'Data Date',
  Region,
  County,
  [ASWS Name],
  [Worker Name],
  [Resource Name],
  [Intake Date],
  CASE WHEN [First Current Placement] is NULL
	THEN 'No'
	ELSE 'Yes'
	END AS 'Children Placed',
  CONVERT(VARCHAR, [First Current Placement], 101) as 'First Current Placement',
  CONVERT(VARCHAR, [Most Recent F2F Contact], 100) as 'Most Recent F2F Contact',
  DATEDIFF(
    DD,
    ISNULL([Most Recent F2F Contact], [Intake Date]),
    GETDATE()
  ) as 'Days Since F2F Contact',
  CONVERT(VARCHAR, [Most Recent Contact], 100) as 'Most Recent Contact',
  DATEDIFF(
    DD,
    ISNULL([Most Recent Contact], [Intake Date]),
    GETDATE()
  ) as 'Days Since Contact'

from (
  
  select coreg.Region,
    coreg.County,
    ISNULL([ASWS Name], 'ASSIGNMENT ERROR') as 'ASWS Name',
    ISNULL([Worker Name], 'ASSIGNMENT ERROR') as 'Worker Name',
    [Resource Name],
	[Intake Date],
    [Placement Start] as 'First Current Placement',
    MAX([Max F2F Narrative]) as 'Most Recent F2F Contact',
	MAX([Max Any Narrative]) as 'Most Recent Contact'
  
  -- Start with a base of all resources with a responsible county, and a 
  -- type with foster parents
  from (
    select RESD_RESOURCE_ID as 'Resource ID',
      RESD_RESOURCE_NAME as 'Resource Name',
      RESD_RESP_CO as 'County Code'
    from MW_RESRC_DIR
    where RESD_TYPE in ('LADPH', 'LCHSP', 'LFOSH', 'RESHM')
      and RESD_RESP_CO is not NULL
	  and RESD_STATUS in ('ACTIV', 'PENDG')
	  and RESD_START_DT is not NULL
	  and (RESD_END_DT >= GETDATE() or RESD_END_DT is NULL)
  ) base

  --Filter out unscreened/screened out inquiries
  inner join (
	select INTS_INTAKE_ID as 'Resource ID'
	from MW_INT_SCREEN
	where INTS_SUP_DECISON = 'I'
  ) screened
  on screened.[Resource ID] = base.[Resource ID]

  -- Add intake date
  left join (
	select INTR_INTAKE_ID as 'Resource ID',
		INTR_INTAKE_DT as 'Intake Date'
	from MW_INT_REPORT
  ) intr
  on intr.[Resource ID] = base.[Resource ID]

  -- Filter for homes with the following active services
  inner join (
	select RESS_RESOURCE_ID as 'Resource ID'
	from MW_RES_SERVICES
	where RESS_END_DT is NULL
		and RESS_SERVICE_TYPE_CD in (
			'9020',		-- Foster Home
			'9040',		-- Emergency Foster Home
			'9060',		-- Therapeutic Foster Home
			'9070',		-- Maternity Home
			'9080',		-- Foster/Adopt Foster Home
			'9110',		-- Adoption Unit Foster Home
			'9130',		-- Adoptive Home (Domestic)
			'9190',		-- Relative Foster Home
			'9210',		-- Teenage Parent Foster Home
			'9275',		-- ICPC - Incoming
			'9290',		-- Child-specific
			'9370'		-- Adoption (International)
		)
  ) ress
  on ress.[Resource ID] = base.[Resource ID]
  
  --Get first current placement
  left join (
    select plac_base.[Resource ID],
		plac_base.[Placement Start]
    
	-- Start with a base of all active placements
	from (
		select PLAC_RESOURCE_ID as 'Resource ID',
		PLAC_RQ_START_DT as 'Placement Start'
		from MW_PLACEMENT
		where PLAC_STATUS = 'A'
	) plac_base
    
	-- Filter to first active placement that is still active
	inner join (
		select distinct MIN(PLAC_RQ_START_DT) as 'First Placement Date',
			PLAC_RESOURCE_ID as 'Resource ID'
		from MW_PLACEMENT
		where PLAC_STATUS = 'A'
		group by PLAC_RESOURCE_ID
	) first_filter
	on first_filter.[First Placement Date] = plac_base.[Placement Start]
		and first_filter.[Resource ID] = plac_base.[Resource ID]
  ) plac_filter
  on plac_filter.[Resource ID] = base.[Resource ID]
  
  -- Add resource parent id's
  left join (
    select reim_base.[Resource ID],
      reim_base.[Person ID]
    from (
      select REIM_RESOURCE_ID as 'Resource ID',
        REIM_PERS_ID as 'Person ID',
        ISN
      from MW_RES_INQUIRY_MEMB
      where REIM_HH_STATUS in ('PRIM', 'SEC')
    ) reim_base
  
    -- Filter on most recent per assignment
    inner join (
      select MAX(ISN) 'Max ISN',
        REIM_RESOURCE_ID as 'Resource ID',
        REIM_HH_STATUS as 'HH Status'
      from MW_RES_INQUIRY_MEMB
      group by REIM_RESOURCE_ID, REIM_HH_STATUS
    ) rec_filter
    on reim_base.ISN = rec_filter.[Max ISN]
    
    union
  
    -- Add parent ID's from MW_RES_RSP_HOME_MEMB as well
    select RRHM_RESOURCE_ID as 'Resource ID',
      RRHM_PERS_ID as 'Person ID'
    from MW_RES_RSP_HOME_MEMB
    where RRHM_ROLE in ('PRIM', 'SEC')
    and RRHM_CHG_STATUS = 'A'
  ) reim
  on reim.[Resource ID] = base.[Resource ID]

  -- Add in worker IDs for licensed homes
  left join (
    select RERW_WORKER_ID as 'Worker ID',
      RERW_RESOURCE_ID as 'Resource ID'
    from MW_RES_RSP_WKR
    where RERW_STATUS = 'A'
  ) lic_home_workers
  on lic_home_workers.[Resource ID] = base.[Resource ID]

  -- Add in recent intake worker
  left join (
	select INWR_INTAKE_ID as 'Resource ID',
		INWR_OWNER as 'Worker ID'
	from MW_INT_WKR_REL
	where INWR_END_DATE is NULL
  ) res_int_workers
  on res_int_workers.[Resource ID] = base.[Resource ID]

  -- Add in worker IDs for resource inquiry
  left join (
    select REIN_WORKER_ID as 'Worker ID',
      REIN_RESOURCE_ID as 'Resource ID'
    from MW_RES_INQUIRY
	where REIN_END_DT is NULL
  ) res_inq_workers
  on res_inq_workers.[Resource ID] = base.[Resource ID]

  -- Add in worker IDs for intakes
  left join (
	select INTS_INTAKE_ID as 'Resource ID',
		INTS_SW_ASSIGNED as 'Worker ID'
	from MW_INT_SCREEN
  ) screen_workers
  on screen_workers.[Resource ID] = base.[Resource ID]

  -- Add in worker information
  left join (
    select [ASWS Name],
      [Worker Name],
      [Worker ID]
    from fod_workers_by_type
  ) wrkr_info
  on wrkr_info.[Worker ID] = ISNULL(lic_home_workers.[Worker ID], 
								ISNULL(res_inq_workers.[Worker ID], 
									ISNULL(screen_workers.[Worker ID], 
										res_int_workers.[Worker ID]
									)
								)
							)

  -- Join in most recent f2f contact for each resource parent by Participant ID
  left join (
    select f2f_narr_id.[Participant ID],
		f2f_narr_id.[Max F2F Narrative]
	from (
		-- Most recent F2F narrative ID per participant/owner
		select napr.[Participant ID],
		max(narr.[Narrative DateTime]) as 'Max F2F Narrative'
		from (
		select NAPR_PARTICIPANT as 'Participant ID',
			NAPR_NARRATIVE_ID as 'Narrative ID'
		from MW_NARR_PART_REL
		--Restrict to resource or home study narratives
		where NAPR_SYSTEM_TYPE in ('H', 'R')
		) napr
		left join (
		select NARR_NARRATIVE_ID as 'Narrative ID',
			NARR_DATETIME as 'Narrative DateTime'
		from MW_NARRATIVE
		where NARR_CONTACT_METHOD = 'FA2FA'
			and NARR_CONTACT_TYPE not in ('470', '480', '600', '625')
		) narr
		on narr.[Narrative ID] = napr.[Narrative ID]
		group by napr.[Participant ID]
	) f2f_narr_id
  ) recent_f2f_contacts
  on recent_f2f_contacts.[Participant ID] = reim.[Person ID]
	and recent_f2f_contacts.[Max F2F Narrative] > intr.[Intake Date]

	 -- Join in most recent contact of any method for each resource parent by Participant ID
  left join (
    select any_narr_id.[Participant ID],
		any_narr_id.[Max Any Narrative]
	from (
		-- Most recent narrative ID per participant/owner
		select napr.[Participant ID],
			max(narr.[Narrative DateTime]) as 'Max Any Narrative'
		from (
			select NAPR_PARTICIPANT as 'Participant ID',
				NAPR_NARRATIVE_ID as 'Narrative ID'
			from MW_NARR_PART_REL
			--Restrict to resource or home study narratives
			where NAPR_SYSTEM_TYPE in ('H', 'R')
		) napr
		left join (
			select NARR_NARRATIVE_ID as 'Narrative ID',
				NARR_DATETIME as 'Narrative DateTime'
			from MW_NARRATIVE
			where NARR_CONTACT_TYPE not in ('470', '480', '600', '625')
		) narr
		on narr.[Narrative ID] = napr.[Narrative ID]
		group by napr.[Participant ID]
	) any_narr_id
  ) recent_any_contacts
  on recent_any_contacts.[Participant ID] = reim.[Person ID]
	and recent_any_contacts.[Max Any Narrative] > intr.[Intake Date]

  -- Add in County and Region where not already provided
  left join (
    select county_code as 'County Code',
      RTRIM(county_name) as 'County',
      RTRIM(region_name) as 'Region'
    from COUNTY_REGION
  ) coreg
  on coreg.[County Code] = base.[County Code]

  group by coreg.Region,
    coreg.County,
    [ASWS Name],
    [Worker Name],
    [Resource Name],
	[Intake Date],
    [Placement Start]
) lic_wrkr_visits

where [Intake Date] is not NULL

order by [Worker Name], [Resource Name]
