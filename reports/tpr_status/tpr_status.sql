select *,
	CASE
		WHEN [Days Since Last Action] > 60 then 2
		WHEN ([Current Status] not in ('Sent To AG', 'Returned From AG') and [Days with Plan of Adoption] > 60) then 2
		WHEN ([Current Status] = 'Not Yet Started' and [Days with Plan of Adoption] > 30) then 2
		WHEN [Days Since Last Action] > 30 then 1
		WHEN ([Current Status] not in ('Sent To AG', 'Returned From AG') and [Days Since Last Action] > 15) then 1
		WHEN ([Current Status] = 'Not Yet Started' and [Days with Plan of Adoption] > 15) then 1
		else 0
	END as 'Indicator Column'
from fod_adoption_status_table