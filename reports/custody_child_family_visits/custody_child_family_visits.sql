select [Data Date]
	,Region
	,County
	,[ASWS Name]
	,[Worker Name]
	,[Case ID]
	,[Child ID]
	,[Child Name]
	,[Case Start]
	,[Custody Start]
	,[Visitor ID]
	,[Visitor Name]
	,[Visitor Type]
	,[Placed Together]
	,[No Contact Order]
	,[Visitation Plan Frequency]
	,[Frequency Type]
	,[Monthly Visits Required]
	,[Monthly Visits Conducted]
	,convert(varchar, [Most Recent Visit], 0) as 'Most Recent Visit'
	,[Days Since Last Visit]
	,[PCT of Visits Held]
	,[Indicator Column]
from fod_family_visit_table